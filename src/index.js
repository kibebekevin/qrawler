import { VersionProvider } from "./Components/Home/Version/VersionContext";
import { AuthContextProvider } from "./Context/AuthContext";
import reportWebVitals from "./Report/reportWebVitals";
import * as serviceWorker from "./Services/ServiceWorker";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import store from "./Redux/Store";
import React from "react";
import App from "./App/App";

const root = ReactDOM.createRoot(document.getElementById("root"));



root.render(
  <AuthContextProvider>
    <Provider store={store}>
      <VersionProvider>
        <App />
      </VersionProvider>
    </Provider>
  </AuthContextProvider>
);
reportWebVitals();

serviceWorker.register();
