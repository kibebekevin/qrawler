import { createStore } from "redux";

// Define the initial state
const initialState = {
  payOtions: false,
  backButtonClicked: false,
  payModal: false,
  settingsAction: false,
  successModal: false,
  confirmModal: false,
  changePassword: false,
  waitingModal: false,
  refreshSpeedModal: false,
  passwordSet: false,
  downloadModal: false,
  downloadurl: "",
  profileModal: false,
  nameEmailModal: false,
  progressPopup:false,
  packageModal:false,
  paymentOptions:"",
  paymentMethod:"",
  platformModal:false,
  processModal:false,
  platformOptions:"",
  cancelRequest:false,
};

// Retrieve the state from localStorage if available
const storedState = localStorage.getItem("reduxState");
const persistedState = storedState ? JSON.parse(storedState) : initialState;

// Define the reducer
const rootReducer = (state = persistedState, action) => {
  switch (action.type) {
    case "BACK_BUTTON_CLICKED":
      const page_no = action.payload.page;
      return {
        ...state,
        backButtonClicked: true,
        payOtions: page_no === "1" ? false : state.payOtions,
        payModal: page_no === "2" ? false : state.payModal,
        settingsAction: page_no === "3" ? false : state.settingsAction,
        successModal: page_no === "4" ? false : state.successModal,
        confirmModal: page_no === "5" ? false : state.confirmModal,
        changePassword: page_no === "6" ? false : state.changePassword,
        waitingModal: page_no === "7" ? false : state.waitingModal,
        refreshSpeedModal: page_no === "8" ? false : state.refreshSpeedModal,
        downloadModal: page_no === "9" ? false : state.downloadModal,
        profileModal: page_no === "10" ? false : state.profileModal,
        nameEmailModal: page_no === "11" ? false : state.nameEmailModal,
        progressPopup: page_no === "12" ? false : state.progressPopup,
        packageModal: page_no === "13" ? false : state.packageModal,
        platformModal: page_no === "14" ? false : state.platformModal,
        processModal: page_no === "15" ? false : state.processModal,
      };

    case "PAY_OPTIONS_OPENED":
      return {
        ...state,
        payOtions: true,
        paymentOptions:action.payload
      };

    case "PAY_MODAL_OPENED":
      return {
        ...state,
        payModal: true,
        paymentMethod:action.payload
      };

    case "SETTINGS_OPENED":
      return {
        ...state,
        settingsAction: true,
      };

    case "SUCCESS_MODAL_OPENED":
      return {
        ...state,
        successModal: true,
      };

    case "CONFIRM_MODAL_OPENED":
      return {
        ...state,
        confirmModal: true,
      };

    case "CHANGE_PASSWORD_ACTION":
      return {
        ...state,
        changePassword: true,
      };

    case "WAITING_MODAL_OPENED":
      return {
        ...state,
        waitingModal: true,
      };

    case "CLOSE_WAITING_MODAL":
      return {
        ...state,
        waitingModal: false,
      };

    case "CHANGE_REFRESH_SPEED":
      return {
        ...state,
        refreshSpeedModal: true,
      };

    case "PASSWORD_SET":
      return {
        ...state,
        passwordSet: true,
      };

    case "PASSWORD_SENT":
      return {
        ...state,
        passwordSet: false,
      };

    case "DOWNLOAD_MODAL_OPENED":
      const { downloadurl } = action.payload;
      return {
        ...state,
        downloadModal: true,
        downloadurl: downloadurl,
      };

    case "PROFILE_MODAL_OPENED":
      return {
        ...state,
        profileModal: true,
      };

    case "NAME_EMAIL_MODAL_OPENED":
      return {
        ...state,
        nameEmailModal: true,
      };
    case 'PROGRESS_POPUP':
      const line = action.payload;
      return {
        ...state,
        progressPopup: true,
        progressData:line
      };
    case 'CLOSE_PROGRESS_POPUP':
        return {
          ...state,
          progressPopup: false,
        };
    case 'PACKAGE_ACTION':
      return {
        ...state,
        packageModal: true,
      };
    case 'PLATFORM_POPUP':
      return {
        ...state,
        platformModal: true,
      };  
    case 'CLOSE_PLATFORM':
      return {
        ...state,
        platformModal:false,
      }; 
    case 'PROCESS_ACTION':
      return {
        ...state,
        processModal: true,
        platformOptions:action.payload
      }; 
    case 'CANCEL_REQUEST':
        return {
          ...state,
          cancelRequest: true,
        };
    case 'RESET_CANCEL_REQUEST':
        return {
          ...state,
          cancelRequest: false,
        };
    case 'CLOSE_PROCESS_MODAL':
      return {
        ...state,
        processModal: false,
      };
    default:
      return state;
  }
};

// Create the Redux store
const store = createStore(rootReducer);

// Subscribe to store changes and save the state to localStorage
store.subscribe(() => {
  const state = store.getState();
  localStorage.setItem("reduxState", JSON.stringify(state));
});

export default store;
