export const backButtonClicked = (page) => ({
    type: 'BACK_BUTTON_CLICKED',
    payload:{
        page:page
    }
});