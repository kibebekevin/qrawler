export const DownloadPopupAction = (downloadurl) => ({
    type: 'DOWNLOAD_MODAL_OPENED',
    payload:{
        downloadurl:downloadurl
    }
});