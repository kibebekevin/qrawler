import Home from "../Components/Home/Home";
import Dummy from "../Components/Home/Dummy/Dummy"
import NoPage from "../Components/Home/NoPage/NoPage"
import Signin from "../Components/Account/Signin/Signin";
import { AuthContext } from "../Context/AuthContext";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css"
import "font-awesome/css/font-awesome.min.css";
import { useContext } from "react";
import "./App.css"

function App() {
  const {currentUser} = useContext(AuthContext)

  const RequireAuth = ({ children }) => {
    return currentUser  ? children : <Navigate to="/" />;
  };

  return (
    <div>
      <BrowserRouter>
        <Routes>
        <Route path="/" element={<Signin />} />
        <Route path="/dummy" element={<Dummy />} />
          <Route path="/dashboard">
            <Route
              index
              element={
                <RequireAuth>
                  <Home />
                </RequireAuth>
              }
            />
          </Route>
          <Route path="*" element={<NoPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
