import React,{useContext,useEffect,useState} from "react";
import {AuthContext} from "../../Context/AuthContext"

function Status() {
    const [text,setText]=useState("...")
    const {currentUser} = useContext(AuthContext)
    useEffect(()=>{
        let isMounted = true;

        if (isMounted) {
            if(currentUser.FreeTrialExpired){
                if(currentUser.IsAccountActive){
                    setText("Unlocked")
                }else{
                    setText("Locked") 
                }
            }else{
                setText("Free Trial")
            }
          }
      
          return () => {
            isMounted = false;
          };
    },[currentUser.FreeTrialExpired, currentUser.IsAccountActive])
  return (
    <>
      <span className="job">
        Account status:{" "}
        <span className="text-info">
          <b style={{color: text==="Unlocked" ? "green" :(text==="Locked" ? "yellow" :"blueviolet")}}>{text}</b>
        </span>
      </span>
    </>
  );
}

export default Status;
