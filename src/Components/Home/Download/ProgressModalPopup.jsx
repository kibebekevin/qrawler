import React,{useEffect,useRef} from "react";
import Loader from "react-dots-loader"
import "react-dots-loader/index.css"
import "./Style.css"
import { useSelector } from "react-redux";

function ProgressModalPopup() {
    const progressData = useSelector((state) => state.progressData);
    const preRef=useRef(null)

    useEffect(()=>{
      let isMounted=true
      if(isMounted){
        scrollToBottom()
      }
      return ()=>{
        isMounted=false
      }
    },[progressData])

    const scrollToBottom=()=>{
      if(preRef.current){
        preRef.current.scrollTop=preRef.current.scrollHeight;
      }
    }
    return (
        <div
          className="snippet-body"
          style={{ position: "absolute", top: "0%",}}
        >
          <div className="card p-3">
            <div>
              <header>
                <h5 className=" text-info">
                   Building {" "}
                  <Loader size={10} color="#4070f4"/>
                </h5>
              </header>
              <hr/>
              <main ref={preRef} className="output-wrapper">{"=> "+progressData.line}</main>
            </div>
          </div>
        </div>
    );
}

export default ProgressModalPopup
