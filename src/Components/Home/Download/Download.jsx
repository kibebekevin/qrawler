import React from "react";
import { useDispatch } from "react-redux";
import {PlatformAction} from "../../../Redux/actions/PlatformAction"

function Download() {
  const dispatch = useDispatch();
  
  const handleButtonClick = () => {
    dispatch(PlatformAction())
  };
  return (
    <>
      <button className="button" onClick={handleButtonClick}>Download</button>
    </>
  );
}

export default Download;
