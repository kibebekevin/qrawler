import React, { useState, useContext } from "react";
import BackButton from "../../Home/Buttons/BackButton";
import { useSelector } from "react-redux";
import { message } from "react-message-popup";
import semver from "semver";
import { auth, db } from "../../../Config/Config";
import {
  doc,
  getDoc,
  setDoc,
  getDocs,
  collection,
  updateDoc,
  where,
  query,
  limit,
} from "firebase/firestore";
import { VersionContext } from "../Version/VersionContext";

function DownloadModal(props) {
  const softwareVersion = useContext(VersionContext);
  const [loading, setLoading] = useState(false);
  const download_url = useSelector((state) => state.downloadurl);
  const handleButtonClick = async () => {
    setLoading(true);
    if (download_url !=="") {
      const userDoc = doc(db, "users", auth.currentUser.uid);
      const userSnapshot = await getDoc(userDoc);
      if (userSnapshot.exists()) {
        const userData = userSnapshot.data();
        const userVersion = userData.SoftwareVersion;
        const userAmount = userData.Amount || 0;
        const userPackage = userData.Package || "Free Version";
        const versionCompare = semver.eq(
          userData.SoftwareVersion.split("v")[1],
          softwareVersion.split("v")[1]
        );
        let newSoftwareVersion;

        if (versionCompare) {
          /* Latest version */
          newSoftwareVersion = userVersion;
        } else {
          /* Old version */
          newSoftwareVersion = softwareVersion;
        }

        await updateDoc(userDoc, {
          IsSoftwareLatest: true,
          SoftwareVersion: newSoftwareVersion,
          IsSoftwareDownloded: true,
        });

        const documentsRef = collection(db, "users");
        const activeDocumentsQuery = query(
          documentsRef,
          where("IsSoftwareDownloded", "==", true)
        );
        const activeDocumentsSnapshot = await getDocs(activeDocumentsQuery);
        const count = activeDocumentsSnapshot.size;

        const qrawlerRef = collection(db, "qrawler");
        const firstDocumentQuery = query(qrawlerRef, limit(1));
        const querySnapshot = await getDocs(firstDocumentQuery);

        if (!querySnapshot.empty) {
          const firstDocument = querySnapshot.docs[0];
          const docRef = doc(db, "qrawler", firstDocument.id);
          const downloadsRef = collection(userDoc, "downloads");

          const nestedData = {
            Package: userPackage,
            Amount: userAmount,
            Date: new Date(),
          };
          
          await updateDoc(docRef, { Downloads: count });
          await setDoc(doc(downloadsRef), nestedData); 

          const link = document.createElement("a");
          link.href = download_url;
          link.setAttribute("download", "qrawler.exe");
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
          setLoading(false);
          message.success("Your download is beginning...");
        }else {
          console.log("No documents found in the collection.");
        }
        const currentUser = JSON.parse(localStorage.getItem("user"));
        const updatedUser = {
          ...currentUser,
          IsSoftwareDownloded: true,
          IsSoftwareLatest: true,
        };
        localStorage.setItem("user", JSON.stringify(updatedUser));
      }else {
        console.log("User document does not exist");
      }
    } else {
      console.error("Failed to download the file");
      message.error("Something went wrong");
      setLoading(false);
    }
  };
  return (
    <div
      className="snippet-body"
      style={{ position: "absolute", top: "40%"}}
    >
      <div className="card p-3 text-center">
        <div>
          <header>
            <h4 className=" text-success">
              <i className="fa fa-check-circle-o"></i> Success
            </h4>
          </header>
          <main>
            <p className="text-left">{props.message}</p>
          </main>
          <div className="d-flex justify-content-center">
            <div className="buttons">
              <BackButton type="9" text="Close" />
              <button
                type="button"
                className="button"
                onClick={handleButtonClick}
              >
                {loading ? "Downloading..." : "Download"}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DownloadModal;