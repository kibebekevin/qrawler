import React from "react";
import "./Style.css";

function CheckInternetConnectivity() {
  return (
    <div
      className="snippet-body d-md-block d-none myconn"
      style={{ position: "absolute", bottom: "10%", left: "1%" }}
    >
      <div className="card p-3 text-center">
        <div className="row">
          <div
            className="col-12 align-items-center justify-content-center"
            style={{ margin: "auto" }}
          >
            <p><i className="fa fa-exclamation-circle text-danger"></i> No Internet connection</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CheckInternetConnectivity;
