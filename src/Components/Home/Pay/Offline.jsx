import React, { useState, useEffect } from "react";
import BackButton from "../Buttons/BackButton";
import "./Page.css";
import Validation from "./Validation";
import { message } from "react-message-popup";

function Offline() {
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState(false);
  const [disabledButton, setDisabledButton] = useState(true);
  const [values, setValues] = useState({ code: "" });

  const handleInput = (event) => {
    const { name, value } = event.target;

    setValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    // Perform validation for the specific input field
    const fieldErrors = Validation({ ...values, [name]: value });
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: fieldErrors[name],
    }));
    setDisabledButton(Object.keys(errors).length > 0);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    setErrors(Validation(values));

    // Check for form errors
    const formErrors = Validation(values);
    if (Object.keys(formErrors).length > 0) {
      setLoading(false);
      message.error("Check your inputs");
      setDisabledButton(true);
      return;
    }

    setDisabledButton(false);
    setLoading(false);
    console.log("All good");
  };
  useEffect(() => {
    if (values.code === "") {
      setDisabledButton(true);
    } else {
      setDisabledButton(false);
    }

    return () => {
      setLoading(false);
    };
  }, [values]);
  return (
    <div>
      <ol style={{ height: "100px", overflowY: "scroll" }}>
        <li>1.Go to your Mpesa Menu</li>
        <li>2.Select lipa na Mpesa</li>
        <li>3.Select paybill</li>
        <li>4.Select business number</li>
        <li>5.Enter business number 404129</li>
        <li>6.Enter account number 404129</li>
        <li>
          7.Confirm details and submit.It should bring the name{" "}
          <b>
            <i>Doublecee Rockers</i>
          </b>
        </li>
        <li>
          8.Copy the Mpesa code and paste here below for payment confirmations
        </li>
      </ol>
      <form
        className="online-offline-page"
        onSubmit={handleSubmit}
        method="post"
      >
        <div className="input-field" style={{ margin: "0 !important" }}>
          <i className="fa fa-clipboard" />
          <input
            type="text"
            placeholder="eg OVBPLNY..."
            name="code"
            onChange={handleInput}
            value={values.code}
          />
          {errors.code && (
            <small className="text-danger error-text">
              <i className="text-danger fa fa-exclamation-circle"></i>{" "}
              {errors.code}
            </small>
          )}
        </div>
        <div className="align-items-center justify-content-center text-center d-flex">
          <div className="buttons">
            <BackButton type="2" text="Close" />
            {loading || disabledButton ? (
              <button type="button" className="button" disabled>
                {disabledButton ? "Submit" : "Please wait..."}
              </button>
            ) : (
              <button type="submit" className="button">
                Submit
              </button>
            )}
          </div>
        </div>
      </form>
    </div>
  );
}

export default Offline;
