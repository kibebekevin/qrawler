import { useState, React, useEffect } from "react";
import "./Page.css";
import BackButton from "../Buttons/BackButton";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import Validation from "./Validation";
import { message } from "react-message-popup";
import { useDispatch,useSelector } from "react-redux";
import { SuccessAction } from "../../../Redux/actions/SuccessAction";
import { ConfirmModal } from "../../../Redux/actions/ConfirmModal";
import axios from "axios";
import { setDoc, getDoc, updateDoc, doc } from "firebase/firestore";
import { auth, db } from "../../../Config/Config";
import generateUniqueId from "../../../key/UniqueId";
import endpoints from "../Endpoints/Endpoints"



function Online() {
  const [errors, setErrors] = useState(false);
  const paymentOptions = useSelector((state) => state.paymentOptions);
  const [disabledButton, setDisabledButton] = useState(true);
  const [values, setValues] = useState({ phone: "" });

  const [loading, setLoading] = useState(false);
  const tranasactionId = generateUniqueId(12);
  const dispatcher = useDispatch();

  const handleInput = (event) => {
    const { name, value } = event.target;

    setValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    // Perform validation for the specific input field
    const fieldErrors = Validation({ ...values, [name]: value });
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: fieldErrors[name],
    }));
    setDisabledButton(Object.keys(errors).length > 0);
  };

  const payment_url =endpoints.production.paymentUrl

  const amount =paymentOptions.price;

  const axiosInstance = axios.create({
    timeout: 5000, // Timeout value in milliseconds
  });


  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    setErrors(Validation(values));

    // Check for form errors
    const formErrors = Validation(values);
    if (Object.keys(formErrors).length > 0) {
      setLoading(false);
      message.error("Check your inputs");
      setDisabledButton(true);
      return;
    }

    setDisabledButton(false);
    confirmAlert({
      title: "Confirm to proceed",
      message: `Are you sure you want to pay for package ${paymentOptions.package} sh ${paymentOptions.price} ?`,
      buttons: [
        {
          label: "Yes",
          onClick: () => handleConfirm(),
        },
        {
          label: "No",
          onClick: () => {
            message.info("Opp cancelled.");
          },
        },
      ],
    });
  };

  const handleConfirm = async () => {
    setLoading(true);
    try {
      const authUser = auth.currentUser;
      const userId = authUser.uid;
      const collection_ref = doc(db, "users", userId);
      const userSnapshot = await getDoc(collection_ref);
      if (userSnapshot.exists()) {
        const retrievedData = userSnapshot.data();
        const phone = retrievedData.Phone;
        await updateDoc(collection_ref, {
          Phone: phone === values.phone ? phone : values.phone,
        })
          .then(async () => {
            await axiosInstance
              .post(payment_url, {
                amount: amount,
                phone: "254" + values.phone.split("0")[1],
                email: authUser.email,
                host: "Qrawler",
                tranasactionId: tranasactionId,
              })
              .then(async (res) => {
                await setDoc(doc(db, "payments", tranasactionId), {
                  UserId: authUser.uid,
                  FullName: authUser.displayName,
                  Email: authUser.email,
                  Type: "Client",
                  TransactionDateInitiated: new Date(),
                  TranasactionId: tranasactionId,
                })
                .then(async () => {
                  setLoading(false);
                  message.success(res.data.CustomerMessage);
                  dispatcher(SuccessAction());
                  setTimeout(()=>{
                    dispatcher(ConfirmModal());
                  },1000)
                })
                .catch((err) => {
                  setLoading(false);
                  message.error("Something went wrong");
                });
              })
              .catch((err) => {
                setLoading(false);
                if (axios.isCancel(err)) {
                  message.error("Request cancelled:", err.message);
                } else if (err.code === "ECONNABORTED") {
                  message.error("Timeout error:", err.message);
                } else if (err.coder === "auth/network-request-failed") {
                  message.error("No network connection");
                } else {
                  setLoading(false);
                  message.error("Something went wrong");
                }
              });
          })
          .catch((err) => {
            setLoading(false);
            message.error("Something went wrong");
          });
      } else {
        message.error("Something went wrong");
      }
    } catch (error) {
      setLoading(false);
      if (error.code === "unavailable") {
        message.error("No network connection");
      } else {
        message.error("Something went wrong");
      }
    }
  };
  useEffect(() => {
    if (values.phone === "") {
      setDisabledButton(true);
    } else {
      setDisabledButton(false);
    }

    return () => {
      setLoading(false);
    };
  }, [errors, values]);

  return (
    <div>
      <h3 className="text-center">Enter credentials</h3>
      <form
        method="post"
        onSubmit={handleSubmit}
        className="online-offline-page"
      >
        <div className="input-field">
          <i className="fa fa-phone" />
          <input
            type="tel"
            placeholder="Phone number"
            name="phone"
            onChange={handleInput}
            value={values.phone}
          />
          {errors.phone && (
            <small className="text-danger error-text">
              <i className="text-danger fa fa-exclamation-circle"></i>{" "}
              {errors.phone}
            </small>
          )}
        </div>
        <br />
        <div className="align-items-center justify-content-center text-center d-flex">
          <div className="buttons">
            <BackButton type="2" text="Close" />
            {loading || disabledButton ? (
              <button type="button" className="button" disabled>
                {disabledButton ? "Submit" : "Please wait..."}
              </button>
            ) : (
              <button type="submit" className="button">
                Submit
              </button>
            )}
          </div>
        </div>
      </form>
      <hr />
      <p>
        <b className="text-info">NB:</b>After submitting this form,it will automatically initiate STK
        push in your mobile phone.
      </p>
    </div>
  );
}

export default Online;
