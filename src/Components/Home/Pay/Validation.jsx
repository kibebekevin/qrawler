export default function Validation(values){
    const errors={}
    const phoneRegex = /^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/i;
    if(values.phone !==undefined){
        if(values.phone === ""){
            errors.phone="Enter your phone number"
        }else if(values.phone.length < 10){
            errors.phone="Enter a valid phone number"  
        }else if(values.phone.length > 10){
            errors.phone="Enter a valid phone number"  
        }else if(phoneRegex.test(values.phone) === false){
            errors.phone="Enter a valid phone number"  
        }
    }

    if(values.code !==undefined){
        if(values.code === ""){
            errors.code="Mpesa code is required"
        }
    }
    
    return errors
}