import React from "react";
import "font-awesome/css/font-awesome.min.css";
import BackButton from "../../Buttons/BackButton";
import Confirm from "./Confirm";
import { useSelector } from "react-redux";

function Success(props) {
  const confirmModal = useSelector((state) => state.confirmModal);
  return (
    <div>
      <div
        className="snippet-body"
        style={{ position: "absolute", top: "10%", left: "10%" }}
      >
        <div className="card p-3 text-center">
          <div>
            <header>
              <h4 className=" text-success">
                <i className="fa fa-check-circle-o"></i> Success
              </h4>
            </header>
            <main>
              <p>{props.message}</p>
            </main>
            <div
              className="align-items-center justify-content-center text-center d-flex"
              style={{ margin: " 0 auto !important" }}
            >
              <div className="buttons">
                <BackButton type="4" text="Ok" />
              </div>
            </div>
          </div>
          {confirmModal && (
            <Confirm message="Payments received successfully." />
          )}
        </div>
      </div>
    </div>
  );
}

export default Success;
