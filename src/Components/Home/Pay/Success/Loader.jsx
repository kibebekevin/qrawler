import React from "react";
import { BallTriangle } from "react-loader-spinner";
import "bootstrap/dist/css/bootstrap.min.css"

function Loader(props) {
  return (
    <div className="spinner">
      <BallTriangle
        height={props.size}
        width={props.size}
        radius={5}
        color="#4158d0"
        ariaLabel="ball-triangle-loading"
        wrapperClass={{ "custom-spinner": true }}
        wrapperStyle=""
        visible={true}
      />
      <br />
      <p className="text-center">{props.text}</p>
      <br />
    </div>
  );
}

export default Loader;
