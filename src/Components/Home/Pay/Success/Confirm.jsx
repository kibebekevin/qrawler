import React, { useState, useContext, useEffect } from "react";
import BackButton from "../../Buttons/BackButton";
import Loader from "./Loader";
import { auth, db } from "../../../../Config/Config";
import { getDoc, doc, updateDoc, onSnapshot } from "firebase/firestore";
import { AuthContext } from "../../../../Context/AuthContext";
import Success from "./Success";
import { useDispatch } from "react-redux";
import { message } from "react-message-popup";

function Confirm(props) {
  const [isVerified, setIsVerified] = useState(false);
  const { currentUser } = useContext(AuthContext);
  const dispatch = useDispatch();
  useEffect(() => {
    const unsubscribe = onSnapshot(
      doc(db, "users", currentUser.uid),
      (snapshot) => {
        const userData = snapshot.data();
        if (userData && userData.IsAccountActive) {
          setIsVerified(true);
          const user = JSON.parse(localStorage.getItem("user"));
          const updatedUser = { ...user, IsAccountActive: true };
          localStorage.setItem("user", JSON.stringify(updatedUser));
          message.error("Payments received successfully.");
        }
      }
    );

    return () => {
      unsubscribe(); // Clean up the listener when the component unmounts
    };
  }, [currentUser.uid, dispatch]);

  const handleButtonClick = async (e) => {
    try {
      const userDocRef = doc(db, "users", currentUser.uid);
      const userDocSnap = await getDoc(userDocRef);
      const userData = userDocSnap.exists() ? userDocSnap.data() : null;
      const hasPaid = userData.HasPaid;
      if (hasPaid) {
        const getUser = doc(db, "users", auth.currentUser.uid);
        await updateDoc(getUser, {
          IsAccountActive: true,
        })
          .then(() => {
            setIsVerified(true);
            const user = JSON.parse(localStorage.getItem("user"));
            const updatedUser = { ...user, IsAccountActive: true };
            localStorage.setItem("user", JSON.stringify(updatedUser));
            message.error("Payments received successfully.");
          })
          .catch((err) => {
            message.error("Unknown error occurred.");
          });
      } else {
        message.error("Kindly check your payments first.");
      }
    } catch (error) {
      message.error("Unknown error occurred.");
    }
  };

  return (
    <>
      <div
        className="snippet-body"
        style={{ position: "absolute", top: "10%", left: "10%" }}
      >
        <div className="card p-3 text-center">
          {!isVerified ? (
            <div>
              <Loader text="Awaiting payments" />
              <div className="buttons">
                <button
                  type="button"
                  className="button"
                  style={{ margin: "auto" }}
                  onClick={handleButtonClick}
                >
                  Confirm Payments
                </button>
              </div>
            </div>
          ) : (
            <div>
              <header>
                <h4 className=" text-success">
                  <i className="fa fa-check-circle-o"></i> Success
                </h4>
              </header>
              <main>
                <p>{props.message}</p>
              </main>
              <div
                className="align-items-center justify-content-center text-center d-flex"
                style={{ margin: " 0 auto !important" }}
              >
                <div className="buttons">
                  <BackButton type="5" text="Ok" />
                </div>
              </div>
            </div>
          )}
          <Success />
        </div>
      </div>
    </>
  );
}

export default Confirm;
