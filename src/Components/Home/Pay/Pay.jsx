import React from "react";
import Offline from "./Offline"
import Online from "./Online"
import Success from "./Success/Success"
import { useSelector } from 'react-redux';

function Pay() {
  const successModal = useSelector(state => state.successModal);
  const paymentMethod = useSelector(state => state.paymentMethod);

  return (
    <div>
      <div
        className="snippet-body"
        style={{ position: "absolute", top: "-40%" }}
      >
        <div className="card p-3">
          <div className={`${successModal ? "overlay" : ""}`}>
            {paymentMethod ===1 ? <Online/> : <Offline/>}
          </div>
          {successModal && <Success message="Request successfully accepted for processing"/>}
        </div>
      </div>
    </div>
  );
}

export default Pay;
