import React from 'react'

function Dummy() {
  return (
    <div id="content-wrapper">
        <a className="skip-link" aria-label="Go to main content" href="#main">Main content</a>
        <header className="navbar fixed" role="banner">
            <div className="logo"><a className="transparent-link logo-link" href="/"><img className="logo-image" alt="Verbit Home page" src="https://platform.verbit.co/assets/verbit-logo-on-dark-symbol-ef5cc911645c609c4f1f406c8d04a9484c8f6c77d7a2ce7348c3386ea09738e3.svg"/></a></div>
            <div className="middle-left-section">
                <h1 className="title" title="My files">My files</h1>
            </div>
            <div className="middle-right-section"></div>
            <ul className="menu">
                <li></li>
                <li></li>
                <li></li>
                <li><span className="dropdown transcriber-dropdown navbar-link"><a aria-expanded="false" aria-haspopup="true" className="dropdown-toggle dropdown-toggle" data-toggle="dropdown" href="/help" role="button">Help</a>
                        <ul className="dropdown-menu">
                            <li><a rel="noreferrer" target="_blank" href="https://verbit.ai/faq-transcriber">FAQ</a></li>
                            <li><a rel="noreferrer" target="_blank" href="https://vimeopro.com/user85458321/verbitai-tutorial-videos">Tutorials</a></li>
                            <li><a rel="noreferrer" target="_blank" href="/saml/verbit_forum">Forum</a></li>
                            <li><a rel="noreferrer" target="_blank" href="/saml/client_support_center">Help Center</a></li>
                        </ul>
                    </span></li>
                <li></li>
                <li>
                    <span className="dropdown navbar-link vpat-dropdown"><a aria-expanded="false" aria-haspopup="true" className="dropdown-toggle" data-toggle="dropdown" href="/" role="button">maldonado@outlook.com</a>
                        <ul className="dropdown-menu">
                            <li><a href="/users/edit">My account</a></li>
                            <li><a href="/payouts">Payments</a></li>
                            <li><a rel="nofollow" data-method="delete" href="/users/sign_out">Sign Out</a></li>
                        </ul>
                    </span>
                </li>
            </ul>
        </header>

        <div id="main" role="main" className="main-content page-wrapper-row" tabindex="-1">
            <div className="alert alert-danger text-center" id="ie-message" style={{display:"none"}}>We no longer support Explorer 11. Please switch to another browser.</div>
            <div data-current-sign-in-timezone="Nairobi" data-formatted-current-sign-in-timezone="(UTC+03:00) Nairobi" data-formatted-selected-timezone="(UTC-06:00) Mountain Time (US &amp; Canada)" data-show="false" data-user-id="143392" data-widget="DifferentTimezoneDetectedAlertWidget">
            </div>



            <div className="col-md-12 banners-wrapper"></div>
            <div className="col-md-10 col-md-offset-1 col-sm-12 margin-top-20">
                <div className="row inline-transcriber-score">
                    <div className="transcriber_score">
                        <div className="your_score score_column">
                            <div className="score_value font-red">3.94</div>
                            <div className="score_name">Your score</div>
                        </div>
                        <div className="target_score score_column">
                            <div className="score_value green">4.3</div>
                            <div className="score_name">Target score</div>
                        </div>
                    </div>
                </div>
                <div className="transcription-job-tables" data-no-jobs-image-url="https://platform.verbit.co/assets/box-623746e41d708b6c8033594446828679bcb264b7c8c5a316eb6a70a545153a4c.png" data-table-view-stages="[&quot;edit&quot;]" data-user-is-transcriber="true" data-widget="TranscriptionJobsIndexWidget">
                    <div className="transcription-job-tables-view-container">
                        <div className="portlet light text-center" style={{marginTop:"30px"}}>
                            <div className="portlet-body">
                                <h1 className="caption-subject font-grey-mint">New jobs will be uploaded shortly</h1>
                                <p>We are working on uploading new jobs {" "} Your patience is appreciated.</p><img src="https://platform.verbit.co/assets/box-623746e41d708b6c8033594446828679bcb264b7c8c5a316eb6a70a545153a4c.png" className="box" alt=""/>
                                <a className="link-class" href="/link/1">link 1</a>
                                <a className="link-class" href="/link/2">link 2</a>
                                <a className="link-class" href="/link/3">link 3</a>
                                <a className="link-class" href="/link/4">link 4</a>
                                <a className="link-class" href="/link/5">link 5</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
            <div className="modal fade" id="already_claimed" tabindex="-1"  aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" type="button" data-dismiss="modal" aria-label="Close">X</button>
                            <h4 className="modal-title font-red"><i className="fa fa-exclamation-triangle"></i> Can not claim job</h4>
                        </div>
                        <div className="modal-body">
                            <p>You can only work on one job at the same time, unassign the job you are currently working on, to claim another.</p>
                        </div>
                        <div className="modal-footer">
                            <button className="btn btn-default" type="button" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade" id="corrections_approval" tabindex="-1" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" type="button" data-dismiss="modal" aria-label="Close">×</button>
                            <h4 className="modal-title font-red"><i className="fa fa-exclamation-triangle"></i> Can not claim job</h4>
                        </div>
                        <div className="modal-body">
                            <p>You cannot work on new jobs untill you approve your previous work's feedback and corrections. Use the link below to view the corrections and approve them.</p>
                            <p><a href="/jobs">Go to Job</a></p>
                        </div>
                        <div className="modal-footer">
                            <button className="btn btn-default" type="button" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="page-wrapper-row" role="contentinfo">
            <div className="page-wrapper-bottom">
                <div className="navbar-fixed-bottom">
                    <div className="page-wrapper-bottom">
                        <div className="page-footer text-center">
                            <div className="container">2023 © Verbatizer By <a className="transparent-link" href="http://verbit.ai">verbit.ai</a><span id="footer-second">|<a className="transparent-link" href="http://verbit.ai">Revolutionizing transcription</a></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Dummy