import React, { useEffect, useContext } from "react";
import Default from "./Default";
import Package from "./Options/Package";
import "./Style.css";
import Avatar from "../../images/myavatar.png";
import { useSelector, useDispatch } from "react-redux";
import Waiting from "./Waiting/Waiting";
import { auth } from "../../Config/Config";
import { AuthContext } from "../../Context/AuthContext";
import { WaitingModal } from "../../Redux/actions/WaitingModal";
import SettingsOptions from "../Home/Settings/SettingsOptions";
import { CloseWaitingModal } from "../../Redux/actions/CloseWaitingModal";
import { VersionContext } from "./Version/VersionContext";
import DownloadModal from "../Home/Download/DownloadModal";
import ProgressModalPopup from "../Home/Download/ProgressModalPopup";
import endpoints from "../Home/Endpoints/Endpoints";
import Platform from "../Home/Platform/Platform";


function Home() {
  const waitingModal = useSelector((state) => state.waitingModal);
  const packageModal = useSelector((state) => state.packageModal);
  const platformModal = useSelector((state) => state.platformModal);
  const settingsAction = useSelector((state) => state.settingsAction);
  const { currentUser } = useContext(AuthContext);
  const dispatch = useDispatch();
  const downloadModal = useSelector((state) => state.downloadModal);
  const progressPopup = useSelector((state) => state.progressPopup);
  const version = useContext(VersionContext);

  useEffect(() => {
    let isMounted = true;
    if (isMounted) {
      if (auth.currentUser && auth.currentUser.uid) {

        const providerData = auth.currentUser.providerData;
        // Check if providerData exists and is not empty
        if (Array.isArray(providerData) && providerData.length > 0) {
          // Check if the providerData array contains any social media provider information
          const usedSocialMedia = providerData.some(
            (provider) =>
              provider.providerId === "google.com" ||
              provider.providerId === "twitter.com" ||
              provider.providerId === "facebook.com"
          );
          if (usedSocialMedia) {
            // User used a social media provider to log in, set IsEmailVerified to false
            const currentUserData = JSON.parse(localStorage.getItem("user"));
            const updatedUser = { ...currentUserData, IsEmailVerified: true};
            localStorage.setItem("user", JSON.stringify(updatedUser));
            dispatch(CloseWaitingModal());
          } else {
            // User used email/password or another login method, check if email is verified
            if (auth.currentUser != null && auth.currentUser.emailVerified) {
              const currentUserData = JSON.parse(localStorage.getItem("user"));
              const updatedUser = { ...currentUserData, IsEmailVerified: true };
              localStorage.setItem("user", JSON.stringify(updatedUser));
              dispatch(CloseWaitingModal());
            } else {
              const user = JSON.parse(localStorage.getItem("user"));
              if (user != null && user.IsEmailVerified) {
                dispatch(CloseWaitingModal());
              } else {
                dispatch(WaitingModal());
              }
            }
          }
        }
      }
    }

    return () => {
      isMounted = false;
    };
  }, [currentUser.uid, dispatch]);

  // Helper function to get the appropriate social media icon based on the providerId
  const getSocialIcon = () => {
    if(currentUser.ProviderName !==""){
      switch (currentUser.ProviderName) {
        case "google.com":
          return "fa-google";
        case "facebook.com":
          return "fa-facebook";
        case "twitter.com":
          return "fa-twitter";
        default:
          return "fa-envelope";
      }
    }else{
      return "fa-envelope";
    }
  };
  const handleImageError = (error) => {
    console.log("image error:",error)
  };
  return (
    <div className="overall-wrapper position-relative">
      <div className="profile-card">
        <div className="imager position-relative">
         {/* Check if the currentUser.PhotoUrl is not empty and use it, else use the default Avatar */}
         {currentUser.PhotoUrl !=="" ? (
            <img src={currentUser.PhotoUrl} alt="" width={150} className="profile-img rounded-circle" onError={handleImageError}/>
          ) : (
            <img src={Avatar} alt="" width={150} className="profile-img" />
          )}

          {/* Apply the socialIcon class to the container */}
          <div className="socialIcon"><i className={`fa ${getSocialIcon()}`}></i></div>
        </div>
        <div
          className={`${
            platformModal ||
            packageModal ||
            waitingModal ||
            settingsAction ||
            downloadModal
              ? "overlay"
              : ""
          }`}
        >
          <Default />
        </div>
        {packageModal && <Package />}
        {waitingModal && <Waiting />}
        {settingsAction && <SettingsOptions />}
        {platformModal && <Platform />}
        {downloadModal && (
          <DownloadModal message="Qrawler software is processed successfully and now is ready for download." />
        )}
        {progressPopup && <ProgressModalPopup />}
        <br />
        <p className="text-center">
          &copy; {new Date().getFullYear()}.{process.env.REACT_APP_SITE_NAME}{" "}
          All rights reserved | Design by{" "}
          <a
            href={endpoints.developer.portfolio}
            rel="noreferrer"
            target="_blank"
          >
            Devme
          </a>
        </p>
        <p className="text-center">{version}</p>
      </div>
    </div>
  );
}

export default Home;
