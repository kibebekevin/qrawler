import React, { createContext, useEffect, useState } from "react";
import { db } from "../../../Config/Config";
import {getDoc,doc } from "firebase/firestore";
import CheckInternetConnectivity from "../internet/CheckInternetConnectivity";

export const VersionContext = createContext();

export const VersionProvider = ({ children }) => {
  const [version, setVersion] = useState("Loading version...");
  const [countdown, setCountdown] = useState(20);
  const [connectivity, setConnectivity] = useState(navigator.onLine);

  useEffect(() => {
    let isMounted = true;

    const fetchData = async () => {
      try {
        const querySnapshot = doc(db, "software", "qrawler1234");
        const userSnapshot = await getDoc(querySnapshot);
        if (isMounted && userSnapshot.exists()) {
          const retrievedData = userSnapshot.data();
          setVersion(retrievedData.Version);
        }
        
      } catch (error) {
        setConnectivity(false)
      }
    };

    fetchData();

    const timer = setInterval(() => {
      if (countdown === 0) {
        clearInterval(timer);
        if (isMounted && version === "Loading version...") {
          setVersion("v1.1.0");
        }
      } else {
        setCountdown(countdown - 1);
      }
    }, 1000);

    const onlineHandler = () => {
      setConnectivity(true);
      document.title = "Qrawler (online)";
    };

    const offlineHandler = () => {
      setConnectivity(false);
      document.title = "Qrawler (offline)";
    };

    window.addEventListener("online", onlineHandler);
    window.addEventListener("offline", offlineHandler);

    return () => {
      clearInterval(timer);
      isMounted = false;
      window.removeEventListener("online", onlineHandler);
      window.removeEventListener("offline", offlineHandler);
    };
  }, [countdown, version]);

  return (
    <VersionContext.Provider value={version}>
      <p className="text-md-left text-center">
        {connectivity ? (
          <i className="fa fa-link text-success"></i>
        ) : (
          <i className="fa fa-unlink text-danger"></i>
        )}
        {version === "Loading version..."
          ? ` Updating version in ${countdown} seconds...`
          : ` Version updated to ${version}`}
      </p>
      {!connectivity && <CheckInternetConnectivity />}
      {children}
    </VersionContext.Provider>
  );
};
