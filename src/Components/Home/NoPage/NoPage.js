import React,{useContext} from "react";
import "./NoPage.css";
import { VersionContext } from "../Version/VersionContext";
import endpoints from "../Endpoints/Endpoints"
function NoPage() {
  const version = useContext(VersionContext);
  return (
    <div className="w3l-loginblock signinform">
      <div className="logo">
        <a className="brand-logo" href="index.html">
          <span className="fa fa-hashtag"></span>Page not found
        </a>
      </div>
      <section className="w3l-errorblock text-center">
        <div>
          <div className="main-cover w3">
            <h4 className="cover-para w3ls">404</h4>
            <h5 className="error">Sorry, We Can't Find That Page You Are Looking For!</h5>
            <p className="">
              The page you are looking for was moved, removed, renamed or never
              existed. Maybe try a search?
            </p>
          </div>
        </div>
      </section>

      <div className="footer">
        <p>
          &copy; {new Date().getFullYear()} {process.env.REACT_APP_SITE_NAME}. All Rights Reserved | Design by{" "}
          <a href={endpoints.developer.portfolio} rel="noreferrer" target="blank">
            Devme
          </a>
        </p>
        <p className="text-center">{version}</p>
      </div>
    </div>
  );
}

export default NoPage;
