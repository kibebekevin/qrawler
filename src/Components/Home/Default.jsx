import React,{useContext} from "react";
import "./Style.css";
import PayButton from "./Buttons/PayButton"
import {AuthContext} from "../../Context/AuthContext"
import Settings from "./Settings/Settings"
import Download from "./Download/Download"
import Status from "./Status"
import endpoints from "../Home/Endpoints/Endpoints"

function Default() {
  const {currentUser} = useContext(AuthContext)
  return (
    <>
        <div className="text-data">
          <span className="name text-capitalize">{currentUser.Name}</span>
          <span className="job">{currentUser.Email}</span>
          <Status/>
        </div>
        <div className="buttons">
            <PayButton/>
            <Download/>
            <Settings/>
        </div>
        <br />
        <p className="text-center">Follow us on social media</p>
        <center>
        <div className="media-buttons">
          <a
            href={endpoints.developer.social.facebook}
            style={{ background: "#4267b2" }}
            target="_blank"
            rel="noreferrer"
            className="link"
          >
            <i className="fa fa-facebook"></i>
          </a>
          <a href={endpoints.developer.social.twitter} style={{ background: "#1da1f2" }} 
          className="link"
          target="_blank"
            rel="noreferrer"
          >
            <i className=" fa fa-twitter"></i>
          </a>
          <a
            href={endpoints.developer.social.instagram}
            style={{ background: " #e1306c" }}
            className="link"
            target="_blank"
            rel="noreferrer"
          >
            <i className="fa fa-instagram"></i>
          </a>
        </div>
        </center>
    </>
  );
}

export default Default;
