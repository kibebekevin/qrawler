import React, { useState, useEffect } from "react";
import { message } from "react-message-popup";
import Validation from "./Validation";
import BackButton from "../../Home/Buttons/BackButton";
import { auth, db } from "../../../Config/Config";
import { updateDoc, doc, onSnapshot } from "firebase/firestore";

function RefreshModal() {
  const [errors, setErrors] = useState(false);
  const [values, setValues] = useState({
    refreshspeed: 2,
    verbitemail: "",
    verbitpassword: "",
  });
  const [loading, setLoading] = useState(false);

  const fetchData = async () => {
    try {
      if (auth.currentUser && auth.currentUser.emailVerified) {
        const userRef = doc(db, "users", auth.currentUser.uid);
        const unsubscribe = onSnapshot(userRef, (docSnapshot) => {
          const data = docSnapshot.data();
          if (data) {
            setValues((prevValues) => ({
              ...prevValues,
              verbitemail: data.VerbitEmail,
              verbitpassword: data.VerbitPassord,
              refreshspeed: data.RefreshSpeed,
            }));
          }
        });
        return unsubscribe;
      }
    } catch (err) {
      console.log("Error fetching data:", err);
    }
  };

  const handleInput = (event) => {
    const { name, value } = event.target;

    setValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    // Perform validation for the specific input field
    const fieldErrors = Validation({ ...values, [name]: value });
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: fieldErrors[name],
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    setErrors(Validation(values));

    // Check for form errors
    const formErrors = Validation(values);
    if (Object.keys(formErrors).length > 0) {
      setLoading(false);
      message.error("Check your inputs");
      return;
    }

    if (auth.currentUser && auth.currentUser.emailVerified) {
      const userRef = doc(db, "users", auth.currentUser.uid);
      try {
        await updateDoc(userRef, {
          VerbitEmail: values.verbitemail,
          VerbitPassword: values.verbitpassword,
          RefreshSpeed: Number(values.refreshspeed),
        })
        .then(()=>{
          setLoading(false);
          message.success("Account settings updated successfully");
          e.target.reset();
        })
        .catch((error) => {
          setLoading(false);
          const errorCode = error.code;
          if (errorCode === "auth/network-request-failed") {
            message.error("No network connection");
          }else {
            message.error("Something went wrong.");
          }
        });
      } catch (err) {
        setLoading(false);
        console.log("Error updating account settings:", err);
        message.error("Something went wrong.");
      }
    }
  };

  useEffect(() => {
    let isMounted = true;

    if (isMounted) {
      fetchData();
    }
    return () => {
      setLoading(false);
      isMounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className="snippet-body" style={{ position: "absolute", top: "-30%" }}>
      <div className="card p-3">
        <h3 className="text-center">|Change Account Settings</h3>
        <form
          method="post"
          onSubmit={handleSubmit}
          className="online-offline-page"
        >
          <div className="input-field">
            <i className="fa fa-envelope" />
            <input
              type="email"
              placeholder="Verbit email address"
              name="verbitemail"
              onChange={handleInput}
              value={values.verbitemail}
            />
            {errors.verbitemail && (
              <small className="text-danger error-text">
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.verbitemail}
              </small>
            )}
          </div>
          <br />
          <div className="input-field">
            <i className="fa fa-lock" />
            <input
              type="password"
              placeholder="Verbit Password"
              name="verbitpassword"
              onChange={handleInput}
              value={values.verbitpassword}
            />
            {errors.verbitpassword && (
              <small className="text-danger error-text">
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.verbitpassword}
              </small>
            )}
          </div>
          <br />
          <div className="input-field">
            <i className="fa fa-refresh" />
            <input
              type="number"
              placeholder="Refresh speed"
              name="refreshspeed"
              onChange={handleInput}
              value={values.refreshspeed}
            />
            {errors.refreshspeed && (
              <small className="text-danger error-text">
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.refreshspeed}
              </small>
            )}
          </div>
          <br />
          <div
            className=" align-items-center justify-content-center text-center d-flex"
            style={{ margin: " 0 auto !important" }}
          >
            <div className="buttons">
              <BackButton type="8" text="Close" />
              {loading  ? (
                <button type="button" className="button" disabled>
                 Please wait...
                </button>
              ) : (
                <button type="submit" className="button">
                  Submit
                </button>
              )}
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default RefreshModal;
