import React from 'react'
import { SettingsAction } from "../../../Redux/actions/SettingsAction";
import { useDispatch } from "react-redux";

function Settings() {
  const dispatch = useDispatch();
  const openSettings=(e)=>{
    dispatch(SettingsAction());
  }
  return (
    <>
        <button type="button" className="button" onClick={openSettings}>
          Settings
        </button>
    </>
  )
}

export default Settings