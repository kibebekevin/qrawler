import React, { useState, useEffect } from "react";
import Validation from "./Validation";
import BackButton from "../Buttons/BackButton";
import { auth, db } from "../../../Config/Config";
import {
  sendEmailVerification,
  updateEmail,
  updateProfile,
} from "firebase/auth";
import { useDispatch, useSelector } from "react-redux";
import { message } from "react-message-popup";
import { updateDoc, doc, onSnapshot } from "firebase/firestore";
import { WaitingModal } from "../../../Redux/actions/WaitingModal";
import Waiting from "../Waiting/Waiting";

function NameEmail() {
  const waitingModal = useSelector((state) => state.waitingModal);
  const [disabledButton, setDisabledButton] = useState(true);
  const [errors, setErrors] = useState(false);
  const [values, setValues] = useState({
    name: auth.currentUser ? auth.currentUser.displayName : "",
    useremail: auth.currentUser ? auth.currentUser.email : "",
  });
  const dispatcher = useDispatch();
  const [loading, setLoading] = useState(false);

  const handleInput = (event) => {
    const { name, value } = event.target;

    setValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    // Perform validation for the specific input field
    const fieldErrors = Validation({ ...values, [name]: value });
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: fieldErrors[name],
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    setErrors(Validation(values));

    // Check for form errors
    const formErrors = Validation(values);
    if (Object.keys(formErrors).length > 0) {
      setLoading(false);
      message.error("Check your inputs");
      setDisabledButton(true);
      return;
    }
    setDisabledButton(false);
    try {
      const user = auth.currentUser;
      const getUser = doc(db, "users", user.uid);
      await updateEmail(user, values.useremail);
      await updateProfile(user, { displayName: values.name })
        .then(async () => {
          await updateDoc(getUser, {
            Email: values.useremail,
            VerbitEmail: values.useremail,
            FirstName: values.name,
          })
            .then(async () => {
              e.target.reset();
              if (!auth.currentUser.emailVerified) {
                await updateDoc(getUser, { IsEmailVerified: false });
                const currentUser = JSON.parse(localStorage.getItem("user"));
                const updatedUser = { ...currentUser, IsEmailVerified: false };
                localStorage.setItem("user", JSON.stringify(updatedUser));
                message.info("Sending verification link...");
                await sendEmailVerification(user)
                  .then(async () => {
                    setLoading(false);
                    dispatcher(WaitingModal());
                    message.success("Account updated.");
                    message.success(
                      `Verification link sent to ${values.useremail}!.`
                    );
                  })
                  .catch((err) => {
                    console.log(err);
                    setLoading(false);
                    message.error("Error sending verification link.");
                  });
              } else {
                setLoading(false);
                message.success("Account updated.");
              }
            })
            .catch((error) => {
              setLoading(false);
              console.log("error 3:", error);
              message.error("Something went wrong.");
              message.error("Please try again.");
            });
        })
        .catch((error) => {
          setLoading(false);
          console.log("error 1:", error);
          message.error("Something went wrong.");
          message.error("Please try again.");
        });
    } catch (error) {
      setLoading(false);
      setDisabledButton(false);
      const errorCode = error.code;
      if (errorCode === "auth/email-already-in-use") {
        message.error("Email address already in use");
      } else if (errorCode === "auth/network-request-failed") {
        message.error("No network connection");
      } else if (errorCode === "auth/requires-recent-login") {
        message.error("Login to make changes.");
      } else {
        message.error("Something went wrong.");
        message.error("Please try again.");
      }

      if (auth.currentUser == null) {
        message.info("Try to login and try again.");
      }
    }
  };

  useEffect(() => {
    if (values.name === "" || values.useremail === "") {
      setDisabledButton(true);
    } else {
      setDisabledButton(false);
    }
    const fetchData = async () => {
      try {
        if (auth.currentUser) {
          const userRef = doc(db, "users", auth.currentUser.uid);
          const unsubscribe = onSnapshot(userRef, (docSnapshot) => {
            const data = docSnapshot.data();
            if (data && data.FirstName && data.Email) {
              setValues((prevValues) => ({
                ...prevValues,
                name: data.FirstName,
                useremail: data.Email,
              }));
            }
          });
          return unsubscribe;
        }
      } catch (err) {
        console.log("Error fetching RefreshSeed:", err);
      }
    };

    fetchData();
    return () => {
      setLoading(false);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className="snippet-body" style={{ position: "absolute", top: "-40%" }}>
      <div className={`card p-3 ${waitingModal ? "overlay" : ""}`}>
        <h3 className="text-center">|Change Profile settings</h3>
        <form
          method="post"
          onSubmit={handleSubmit}
          className="online-offline-page"
        >
          <div className="input-field">
            <i className="fa fa-user" />
            <input
              type="text"
              placeholder="Full Name"
              name="name"
              onChange={handleInput}
              value={values.name}
            />
            {errors.name && (
              <small className="text-danger error-text">
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.name}
              </small>
            )}
          </div>
          <br />
          <div className="input-field">
            <i className="fa fa-email" />
            <input
              type="email"
              placeholder="Email address"
              name="useremail"
              onChange={handleInput}
              value={values.useremail}
            />
            {errors.useremail && (
              <small className="text-danger error-text">
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.useremail}
              </small>
            )}
          </div>
          <br />
          <div
            className="align-items-center justify-content-center text-center d-flex"
            style={{ margin: " 0 auto !important" }}
          >
            <div className="buttons">
              <BackButton type="11" text="Close" />
              {loading || disabledButton ? (
                <button type="button" className="button" disabled>
                  {disabledButton ? "Submit" : "Please wait..."}
                </button>
              ) : (
                <button type="submit" className="button">
                  Submit
                </button>
              )}
            </div>
          </div>
        </form>
      </div>
      {waitingModal && <Waiting />}
    </div>
  );
}
export default NameEmail;
