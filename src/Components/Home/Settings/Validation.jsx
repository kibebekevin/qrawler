export default function Validation(values){
    const errors={}
    const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(values.refreshspeed !==undefined){
      if(values.refreshspeed === ""){
          errors.refreshspeed="Your automation refresh speed"
      }
    }

    if(values.verbitemail !==undefined){
      if (values.verbitemail === "") {
        errors.verbitemail = "Enter your verbit email address";
      } else if (emailRegex.test(values.verbitemail) === false) {
        errors.verbitemail = "Enter a valid email address";
      }
    }
  

    if(values.verbitpassword !==undefined){
      if (values.verbitpassword === "") {
        errors.verbitpassword = "Your verbit assword is required";
      }
    }

    if(values.name !==undefined){
      if(values.name === ""){
        errors.name = "Your name is required";
      }
    }

    if(values.useremail !==undefined){
      if (values.useremail === "") {
        errors.useremail = "Enter your email address";
      } else if (emailRegex.test(values.useremail) === false) {
        errors.useremail = "Enter a valid email address";
      }
    }
    
    return errors
}