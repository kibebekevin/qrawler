import React,{useState} from "react";
import { useDispatch,useSelector } from "react-redux";
import PasswordReset from "../../Account/Password_reset/PasswordReset"
import BackButton from "../Buttons/BackButton";
import "./Style.css"
import { ChangePasswordAction } from "../../../Redux/actions/ChangePasswordAction";
import { NameEmailAction } from "../../../Redux/actions/NameEmailAction";
import NameEmail from "./NameEmail"

function Profile() {
    const [isHover, setIsHover] = useState(false);
    const [isHover1, setIsHover1] = useState(false);
    const dispatcher = useDispatch();
    const changePassword = useSelector((state) => state.changePassword);
    const nameEmailModal = useSelector((state) => state.nameEmailModal);

  const setIsHovered = () => {
    setIsHover(true);
  };
  const setIsUnHovered = () => {
    setIsHover(false);
  };
  const setIsHovered1 = () => {
    setIsHover1(true);
  };
  const setIsUnHovered1 = () => {
    setIsHover1(false);
  };
  
  const openModal = (option) => {
    if (option === 1) {
      setIsHover(true);
      setIsHover1(false);
      dispatcher(NameEmailAction());
    } else if (option === 2) {
      setIsHover(false);
      setIsHover1(true);
        dispatcher(ChangePasswordAction());
    }
  };
    return (
        <>
        <div
        className="snippet-body"
        style={{ position: "absolute", top: "-10%" }}
      >
        <div>
          <div className={`card ${nameEmailModal ? "overlay" : ""}`}>
            <div className="p-3">
              <div
                style={{ cursor: "pointer" }}
                className={`${isHover ? "first" : "second"} p-3 d-flex`}
                onMouseEnter={setIsHovered}
                onMouseLeave={setIsUnHovered}
                onClick={() => openModal(1)}
              >
                <div className="border-left pl-2">
                  <span className="head"><i className="fa fa-user"></i> Change profile details </span>
                </div>
              </div>
            </div>
            <div className="p-3">
              <div
                style={{ cursor: "pointer" }}
                className={`${isHover1 ? "first" : "second"} p-3 d-flex`}
                onMouseEnter={setIsHovered1}
                onMouseLeave={setIsUnHovered1}
                onClick={() => openModal(2)}
              >
                <div className="border-left pl-2">
                  <span className="head"><i className="fa fa-lock"></i> Change password </span>
                </div>
              </div>
            </div> 
            <div className="d-flex justify-content-center px-3 pt-4 pb-3">
              <div className="buttons">
                <BackButton type="10" text="Close"/>
              </div>
            </div>
          </div>
          {changePassword && <PasswordReset/>}
          {nameEmailModal && <NameEmail/> }
        </div>
      </div>
        </>
      );
}

export default Profile
