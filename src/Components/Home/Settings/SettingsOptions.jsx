import React,{useState,useContext} from "react";
import "bootstrap/dist/css/bootstrap.min.css"
import "font-awesome/css/font-awesome.min.css"
import BackButton from "../Buttons/BackButton";
import "./Style.css"
import { auth } from "../../../Config/Config";
import { message } from "react-message-popup";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../../Context/AuthContext";
import { RefreshSpeedAction } from "../../../Redux/actions/RefreshSpeedAction";
import { useDispatch,useSelector } from "react-redux";
import { ProfileModalAction } from "../../../Redux/actions/ProfileModalAction";
import RefreshModal from "./RefreshModal";
import {backButtonClicked} from "../../../Redux/actions/BackButtonAction"
import Profile from "./Profile"


function SettingsOptions() {
  const [isHover, setIsHover] = useState(false);
  const [isHover1, setIsHover1] = useState(false);
  const [isHover2, setIsHover2] = useState(false);
  const navigate = useNavigate();
  const { dispatch } = useContext(AuthContext);
  const dispatcher = useDispatch();
  const [loading, setLoading] = useState(false);
  const { currentUser } = useContext(AuthContext);
  const refreshSpeedModal = useSelector((state) => state.refreshSpeedModal);
  const profileModal = useSelector((state) => state.profileModal);

  const setIsHovered = () => {
    setIsHover(true);
  };
  const setIsUnHovered = () => {
    setIsHover(false);
  };
  const setIsHovered1 = () => {
    setIsHover1(true);
  };
  const setIsUnHovered2 = () => {
    setIsHover1(false);
  };
  const setIsHovered3 = () => {
    setIsHover2(true);
  };
  const setIsUnHovered3 = () => {
    setIsHover2(false);
  };

  const openModal = (option) => {
    if (option === 1) {
      setIsHover(true);
      setIsHover1(false);
      dispatcher(RefreshSpeedAction());
    } else if (option === 2) {
      setIsHover(false);
      setIsHover1(true);
      setLoading(true);
      auth
      .signOut()
      .then(() => {
        dispatcher(backButtonClicked("3"));
        dispatch({ type: "Logout", payload: currentUser });
        message.success("Logged out successfully.");
        setLoading(false);
        navigate("/");
      })
      .catch(() => {
        setLoading(false);
        message.error("Error while logging you out.");
      });
    }else{
      dispatcher(ProfileModalAction())
    }
  };
  return (
    <>
    <div
    className="snippet-body"
    style={{ position: "absolute", top: "30%" }}
  >
    <div>
      <div className={`card ${refreshSpeedModal || profileModal ? "overlay" : ""}`}>
        <div className="p-3">
          <div
            style={{ cursor: "pointer" }}
            className={`${isHover2 ? "first" : "second"} p-3 d-flex`}
            onMouseEnter={setIsHovered3}
            onMouseLeave={setIsUnHovered3}
            onClick={() => openModal(3)}
          >
            <div className="border-left pl-2">
              <span className="head"><i className="fa fa-user"></i> Change account settings </span>
            </div>
          </div>
        </div>
        <div className="p-3">
          <div
            style={{ cursor: "pointer" }}
            className={`${isHover ? "first" : "second"} p-3 d-flex`}
            onMouseEnter={setIsHovered}
            onMouseLeave={setIsUnHovered}
            onClick={() => openModal(1)} 
          >
            <div className="border-left pl-2">
              <span className="head"><i className="fa fa-lock"></i> Verbit settings </span>
            </div>
          </div>
        </div> 
        <div className="p-3">
          <div
            style={{ cursor: "pointer" }}
            className={`${isHover1 ? "first" : "second"} d-flex p-3`}
            onMouseEnter={setIsHovered1}
            onMouseLeave={setIsUnHovered2}
            onClick={() => openModal(2)}
          >
            <div className="border-left pl-3">
              <span className="head"><i className="fa fa-arrow-right"></i> {loading ? "Logging out..." : "Logout"}</span>
            </div>
          </div>
        </div>

        <div className="d-flex justify-content-center px-3 pt-4 pb-3">
          <div className="buttons">
            <BackButton type="3" text="Close"/>
          </div>
        </div>
      </div>
      {refreshSpeedModal && <RefreshModal/>}
      {profileModal && <Profile/>}
    </div>
  </div>
    </>
  );
}

export default SettingsOptions;
