import React from "react";
import { useDispatch } from "react-redux";
import { backButtonClicked } from "../../../Redux/actions/BackButtonAction";

const BackButton = (props) => {
  const dispatch = useDispatch();
  const handleBackButtonClick = () => {
    dispatch(backButtonClicked(props.type));
   
  };

  return (
    <>
      <button
        type="button"
        className="button"
        onClick={handleBackButtonClick}
      >
        {props.text}
      </button>
    </>
  );
};

export default BackButton;
