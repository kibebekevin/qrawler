import React from "react";
import { PackageAction } from "../../../Redux/actions/PackageAction";
import { useDispatch } from "react-redux";

function PayButton() {
  const dispatch = useDispatch();

  const handlePayButtonClick = () => {
    dispatch(PackageAction());
  };
  return (
    <>
      <button className="button" onClick={handlePayButtonClick}>Pay</button>
    </>
  );
}

export default PayButton;
