import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {CancelAction} from "../../../Redux/actions/CancelAction";


function Process() {
  const [loading, setLoading] = useState(false);
  const processModal = useSelector((state) => state.processModal)
  const dispatch = useDispatch();

  const handleCancelClick = (e) => {
    setLoading(true)
    dispatch(CancelAction())
    if(!processModal){
      setLoading(false)
    }
  };

  return (
    <div>
      <div
        className="snippet-body"
        style={{ position: "absolute", bottom: "0%" }}
      >
        <div className="card p-3 text-center">
          <div className="row">
            <div
              className="col-6 align-items-center justify-content-center pb-2"
              style={{ margin: "auto" }}
            >
              <p><i className="fa fa-refresh text-info"></i> Processing...</p>
            </div>
            <div className="col-6" style={{ marginTop: "-7%" }}>
              <div className="buttons">
                {loading ? (
                  <button className="button" disabled>
                    Cancelling...
                  </button>
                ) : (
                  <button className="button" onClick={handleCancelClick} disabled>
                    Cancel
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Process;
