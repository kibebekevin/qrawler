import React, { useState } from "react";
import "./Options.css";
import BackButton from "../Buttons/BackButton";
import Pay from "../Pay/Pay";
import { useDispatch, useSelector } from "react-redux";
import packages from "../Packages/Packages"
import Options from "./Options";
import {PayOptions} from "../../../Redux/actions/PayOptions";


function Package() {
    const [isHover, setIsHover] = useState(false);
    const payOtions = useSelector((state) => state.payOtions)
    const dispatch = useDispatch();
    const [isHover1, setIsHover1] = useState(false);
    const [isOption1Checked, setIsOption1Checked] = useState(false);
    const [isOption2Checked, setIsOption2Checked] = useState(true);
    const payModal = useSelector((state) => state.payModal);
  
    const setIsHovered = () => {
      setIsHover(true);
    };
    const setIsUnHovered = () => {
      setIsHover(false);
    };
    const setIsHovered1 = () => {
      setIsHover1(true);
    };
    const setIsUnHovered2 = () => {
      setIsHover1(false);
    };
    const openModal = (options) => {
        dispatch(PayOptions(options));
        if (options.value === 1) {
        setIsOption1Checked(true);
        setIsHover(true);
        setIsHover1(false);
        setIsOption2Checked(false);
      } else if (options.value === 2) {
        setIsOption1Checked(false);
        setIsOption2Checked(true);
        setIsHover(false);
        setIsHover1(true);
      }
    };
    return (
        <>
          <div
            className="snippet-body"
            style={{ position: "absolute", top: "33%" }}
          >
            <div className={`card ${payOtions ? "overlay" : ""}`}>
                <h5 className="text-center pt-2">Choose plan</h5>
              <div>
                <div className="py-2  px-3">
                  <div
                    style={{ cursor: "pointer" }}
                    className={`${isHover ? "first" : "second"} pl-2 d-flex py-2`}
                    onMouseEnter={setIsHovered}
                    onMouseLeave={setIsUnHovered}
                    onClick={() => openModal({value:1,package:packages.site_packages.lite.package_name,price:packages.site_packages.lite.price})}
                  >
                    <div className="form-check">
                      <input
                        type="radio"
                        name="optradio"
                        className="form-check-input mt-3 dot  pl-2"
                        checked={isOption1Checked}
                        readOnly
                      />
                    </div>
                    <div className="border-left pl-2  pt-3 pb-3">
                      <div>
                        <span className="dollar"><i className="fa fa-rocket"></i> </span>
                        <span className="amount">{packages.site_packages.lite.package_name}</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="py-2  px-3">
                  <div
                    style={{ cursor: "pointer" }}
                    className={`${isHover1 ? "first" : "second"} pl-2 d-flex py-2`}
                    onMouseEnter={setIsHovered1}
                    onMouseLeave={setIsUnHovered2}
                    onClick={() => openModal({value:2,package:packages.site_packages.pro.package_name,price:packages.site_packages.pro.price})}
                  >
                    <div className="form-check">
                      <input
                        type="radio"
                        name="optradio"
                        className="form-check-input mt-3 dot  pl-2"
                        checked={isOption2Checked}
                        readOnly
                      />
                    </div>
                    <div className="border-left pl-2 pt-3 pb-3">
                        <div>
                            <span className="dollar"><i className="fa fa-fire"></i> </span>
                            <span className="amount">{packages.site_packages.pro.package_name}</span>
                        </div>
                    </div>
                  </div>
                </div>
    
                <div className="d-flex justify-content-center px-3 pt-4 pb-3">
                  <div className="buttons">
                    <BackButton type="13" text="Close"/>
                  </div>
                </div>
              </div>
            </div>
            {payOtions && <Options /> }
            {payModal && (
                  <Pay type={isOption1Checked ? "online" : "offline"} />
                )}
          </div>
        </>
      );
}

export default Package