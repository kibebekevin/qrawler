import React, { useState } from "react";
import "./Options.css";
import BackButton from "../Buttons/BackButton";
import Pay from "../Pay/Pay";
import { useDispatch, useSelector } from "react-redux";
import { PayModal } from "../../../Redux/actions/PayModal";
import { message } from "react-message-popup";



function Options() {
  const [isHover, setIsHover] = useState(false);
  const dispatch = useDispatch();
  const [isHover1, setIsHover1] = useState(false);
  const [isOption1Checked, setIsOption1Checked] = useState(false);
  const [isOption2Checked, setIsOption2Checked] = useState(true);
  const payModal = useSelector((state) => state.payModal);
  const paymentOptions = useSelector((state) => state.paymentOptions);

  const setIsHovered = () => {
    setIsHover(true);
  };
  const setIsUnHovered = () => {
    setIsHover(false);
  };
  const setIsHovered1 = () => {
    setIsHover1(true);
  };
  const setIsUnHovered2 = () => {
    setIsHover1(false);
  };
  const openModal = (option) => {
    if(paymentOptions.package ==="lite"){
      dispatch(PayModal(option));
    }else{
      message.info("Plan not implemented yet.")
    }
    if (option === 1) {
      setIsOption1Checked(true);
      setIsHover(true);
      setIsHover1(false);
      setIsOption2Checked(false);
    } else if (option === 2) {
      setIsOption1Checked(false);
      setIsOption2Checked(true);
      setIsHover(false);
      setIsHover1(true);
    }
  };
  return (
    <>
      <div
        className="snippet-body"
        style={{ position: "absolute", top: "-10%" }}
      >
        <div className={`card pt-3 ${payModal ? "overlay" : ""}`}>
          <h5 className="text-center pt-3">Plan:<span className="text-info">{paymentOptions.package}</span></h5>
          <div>
            <div className="py-2  px-3">
              <div
                style={{ cursor: "pointer" }}
                className={`${isHover ? "first" : "second"} pl-2 d-flex py-2`}
                onMouseEnter={setIsHovered}
                onMouseLeave={setIsUnHovered}
                onClick={() => openModal(1)}
              >
                <div className="form-check">
                  <input
                    type="radio"
                    name="optradio"
                    className="form-check-input mt-3 dot  pl-2"
                    checked={isOption1Checked}
                    readOnly
                  />
                </div>
                <div className="border-left pl-2">
                  <span className="head">Online Lipa na Mpesa </span>
                  <div>
                    <span className="dollar">Ksh </span>
                    <span className="amount">{paymentOptions.price}</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="py-2  px-3">
              <div
                style={{ cursor: "pointer" }}
                className={`${isHover1 ? "first" : "second"} pl-2 d-flex py-2`}
                onMouseEnter={setIsHovered1}
                onMouseLeave={setIsUnHovered2}
                onClick={() => openModal(2)}
              >
                <div className="form-check">
                  <input
                    type="radio"
                    name="optradio"
                    className="form-check-input mt-3 dot  pl-2"
                    checked={isOption2Checked}
                    readOnly
                  />
                </div>
                <div className="border-left pl-3">
                  <span className="head">Manual top up</span>
                  <div>
                    <span className="dollar">Ksh </span>
                    <span className="amount">{paymentOptions.price}</span>
                  </div>
                </div>
              </div>
            </div>

            <div className="d-flex justify-content-center px-3 pt-4 pb-3">
              <div className="buttons">
                <BackButton type="1" text="Close"/>
              </div>
            </div>
          </div>
        </div>
        {payModal && (<Pay/>)}
      </div>
    </>
  );
}

export default Options;
