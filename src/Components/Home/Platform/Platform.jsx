import React, { useState, useEffect, useContext } from "react";
import "./Platform.css";
import BackButton from "../Buttons/BackButton";
import { useDispatch, useSelector } from "react-redux";
import { ProcessAction } from "../../../Redux/actions/ProcessAction";
import Process from "../Process/Process";
import io from "socket.io-client";
import endpoints from "../Endpoints/Endpoints";
import { message } from "react-message-popup";
import { CloseProgressPopup } from "../../../Redux/actions/CloseProgressPopup";
import { ProgressPopup } from "../../../Redux/actions/ProgressPopup";
import { CloseProcessModal } from "../../../Redux/actions/CloseProcessModal";
import { auth, db } from "../../../Config/Config";
import { doc, getDoc, updateDoc } from "firebase/firestore";
import { VersionContext } from "../Version/VersionContext";
import axios from "axios";
import { DownloadPopupAction } from "../../../Redux/actions/DownloadPopupAction";
import { ClosePlatformAction } from "../../../Redux/actions/ClosePlatformAction";
import { PlatformAction } from "../../../Redux/actions/PlatformAction";
import { ResetCancelRequest } from "../../../Redux/actions/ResetCancelRequest";

function Platform() {
  const [processOptions, setProcessOptions] = useState(null);
  const [isHover, setIsHover] = useState(false);
  const [isHover1, setIsHover1] = useState(false);
  const softwareVersion = useContext(VersionContext);
  const [isHover2, setIsHover2] = useState(false);
  const host = endpoints.production.host;
  const serverUrl = endpoints.production.serverUrl;
  const [isOption1Checked, setIsOption1Checked] = useState(false);
  const [isOption2Checked, setIsOption2Checked] = useState(true);
  const [isOption3Checked, setIsOption3Checked] = useState(true);
  const [socketConnected, setSocketConnected] = useState(false);
  const processModal = useSelector((state) => state.processModal);
  const cancelRequest = useSelector((state) => state.cancelRequest);
  const dispatch = useDispatch();

  const setIsHovered = () => {
    setIsHover(true);
  };
  const setIsUnHovered = () => {
    setIsHover(false);
  };
  const setIsHovered1 = () => {
    setIsHover1(true);
  };
  const setIsUnHovered1 = () => {
    setIsHover1(false);
  };
  const setIsHovered2 = () => {
    setIsHover2(true);
  };
  const setIsUnHovered2 = () => {
    setIsHover2(false);
  };

  const cancelTokenSource = axios.CancelToken.source();

  const openModal = (options) => {
    setProcessOptions(options);

    if (options.value === 1) {
      setIsOption1Checked(true);
      setIsOption2Checked(false);
      setIsOption3Checked(false);
      setIsHover(true);
      setIsHover1(false);
      setIsHover2(false);
    } else if (options.value === 2) {
      setIsOption1Checked(false);
      setIsOption2Checked(true);
      setIsOption3Checked(false);
      setIsHover1(true);
      setIsHover(false);
      setIsHover2(false);
    } else if (options.value === 3) {
      setIsOption1Checked(false);
      setIsOption2Checked(false);
      setIsOption3Checked(true);
      setIsHover(false);
      setIsHover1(false);
      setIsHover2(true);
    }

    processRequest(options);
  };

  useEffect(() => {
    const socket = io(host);
    dispatch(ResetCancelRequest());

    if (cancelRequest) {
      cancelTokenSource.cancel("Request canceled by the user");
      dispatch(ClosePlatformAction());
    }

    socket.on("connect", () => {
      message.success("Connected.");
      setSocketConnected(true);
    });

    socket.on("conversion_success", () => {
      message.info("Done preparing.");
    });

    socket.on("error", (error) => {
      if (error !== "Disconnected from the server.") {
        message.error("Something went wrong");
      }
      dispatch(CloseProgressPopup());
      dispatch(CloseProcessModal());
    });

    socket.on("progress_update", (line) => {
      const data = {
        line: line,
      };
      if (line !== null && line !== "" && line !== undefined) {
        dispatch(ProgressPopup(data));
        dispatch(ProcessAction(processOptions));
      } else {
        dispatch(CloseProgressPopup());
        dispatch(CloseProcessModal());
      }
    });

    socket.on("disconnect", () => {
      setSocketConnected(false);
      dispatch(CloseProgressPopup());
      dispatch(CloseProcessModal());
      message.info("Disconnected.");
    });

    return () => {
      socket.disconnect();
      cancelTokenSource.cancel("Component unmounted");
    };
  }, [cancelRequest,processOptions,dispatch,host]);

  const processRequest = async (options) => {
    try {
      const userDoc = doc(db, "users", auth.currentUser.uid);

      const userSnapshot = await getDoc(userDoc);

      if (userSnapshot.exists() && socketConnected) {
        const userData = userSnapshot.data();

        if (softwareVersion !== "Loading version...") {
          dispatch(ProcessAction(options));
          try {
            await axios
              .post(
                serverUrl,
                {
                  userId: auth.currentUser.uid,
                  softwareId: userData.SoftwareId,
                  softwareVersion: softwareVersion,
                  platform: options.platform,
                },
                {
                  timeout: 0,
                  cancelToken: cancelTokenSource.token, // Attach the cancel token
                }
              )
              .then(async (res) => {
                const userDoc = doc(db, "users", auth.currentUser.uid);
                const userSnapshot = await getDoc(userDoc);
                if (userSnapshot.exists()) {
                  await updateDoc(userDoc, {
                    DownloadAgain: true,
                  })
                    .then(() => {
                      message.success("Download ready");
                      dispatch(CloseProgressPopup());
                      dispatch(CloseProcessModal());
                      dispatch(DownloadPopupAction(res.data.download_url));
                    })
                    .catch((error) => {
                      dispatch(CloseProgressPopup());
                      dispatch(CloseProcessModal());
                      message.error("something went wrong");
                    });
                }
              })
              .catch((err) => {
                dispatch(CloseProgressPopup());
                dispatch(CloseProcessModal());
                if (axios.isCancel(err)) {
                  message.error("Request canceled");
                } else if (err.code === "ECONNABORTED") {
                  message.error("Timeout error");
                } else if (err.coder === "auth/network-request-failed") {
                  message.error("No network connection");
                } else {
                  message.error(err.message);
                }
              });
          } catch (error) {
            dispatch(CloseProgressPopup());
            dispatch(CloseProcessModal());
            console.log("Error:", error);
            console.log("Failed to initiate the conversion process.");
          }
        } else {
          message.info("Version loading");
        }
      } else {
        dispatch(CloseProgressPopup());
        dispatch(CloseProcessModal());
        console.log("User document does not exist");
      }
    } catch (error) {
      const errorCode = error.code;
      if (errorCode === "unavailable") {
        message.error("No network connection");
      } else {
        message.error("Something went wrong");
      }
    }
  };

  return (
    <>
      <div className="snippet-body" style={{ position: "absolute", top: "0%" }}>
        <div className={`card ${processModal ? "overlayj" : ""}`}>
          <h5 className="text-center pt-2">Choose platform</h5>
          <div>
            <div className={`py-2 px-3 ${!socketConnected ? "no_connectivity" : ""}`}>
              <div
                style={{ cursor: "pointer" }}
                className={`${isHover ? "first" : "second"} pl-2 d-flex py-2`}
                onMouseEnter={setIsHovered}
                onMouseLeave={setIsUnHovered}
                onClick={() => openModal({ value: 1, platform: "windows" })}
              >
                <div className="form-check">
                  <input
                    type="radio"
                    name="optradio"
                    className="form-check-input mt-3 dot pl-2"
                    checked={isOption1Checked}
                    readOnly
                  />
                </div>
                <div className="border-left pl-2 pt-3 pb-3">
                  <div>
                    <span className="dollar">
                      <i className="fa fa-windows"></i>{" "}
                    </span>
                    <span className="amount">Windows</span>
                  </div>
                </div>
              </div>
            </div>

            <div className={`py-2 px-3 no_connectivity ${!socketConnected ? "no_connectivity" : ""}`}>
              <div
                style={{ cursor: "pointer" }}
                className={`${isHover1 ? "first" : "second"} pl-2 d-flex py-2`}
                onMouseEnter={setIsHovered1}
                onMouseLeave={setIsUnHovered1}
                onClick={() => openModal({ value: 2, platform: "linux" })}
              >
                <div className="form-check">
                  <input
                    type="radio"
                    name="optradio"
                    className="form-check-input mt-3 dot pl-2"
                    checked={isOption2Checked}
                    readOnly
                  />
                </div>
                <div className="border-left pl-2 pt-3 pb-3">
                  <div>
                    <span className="dollar">
                      <i className="fa fa-linux"></i>{" "}
                    </span>
                    <span className="amount">Linux</span>
                  </div>
                </div>
              </div>
            </div>

            <div className={`py-2 px-3 no_connectivity ${!socketConnected ? "no_connectivity" : ""}`}>
              <div
                style={{ cursor: "pointer" }}
                className={`${isHover2 ? "first" : "second"} pl-2 d-flex py-2`}
                onMouseEnter={setIsHovered2}
                onMouseLeave={setIsUnHovered2}
                onClick={() => openModal({ value: 3, platform: "macos" })}
              >
                <div className="form-check">
                  <input
                    type="radio"
                    name="optradio"
                    className="form-check-input mt-3 dot pl-2"
                    checked={isOption3Checked}
                    readOnly
                  />
                </div>
                <div className="border-left pl-2 pt-3 pb-3">
                  <div>
                    <span className="dollar">
                      <i className="fa fa-apple"></i>{" "}
                    </span>
                    <span className="amount">Mac Os</span>
                  </div>
                </div>
              </div>
            </div>

            <div className="d-flex justify-content-center px-3 pt-4 pb-3">
              <div className="buttons">
                <BackButton type="14" text="Close" />
              </div>
            </div>
          </div>
        </div>
        {processModal && <Process />}
      </div>
    </>
  );
}

export default Platform;
