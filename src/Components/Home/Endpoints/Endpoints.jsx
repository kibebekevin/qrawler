const endpoints={
    "production":{
        "host":"https://qrawler-python-c23f92a02b26.herokuapp.com/",
        "serverUrl":"https://qrawler-python-c23f92a02b26.herokuapp.com/process/v1/downloads",
        "completeUrl":"https://qrawler-python-c23f92a02b26.herokuapp.com/process/v1/downloads/complete",
        "paymentUrl":"https://pesaflows-nodejs.herokuapp.com/process/payments/initiate/stk/push",
        "downloadUrl":"https://qrawler-python-c23f92a02b26.herokuapp.com/process/v1/downloads/download",
    },
    "testing":{
        "host":"http://localhost:5000",
        "serverUrl":"http://localhost:5000/process/v1/downloads",
        "completeUrl":"http://localhost:5000/process/v1/downloads/complete",
        "paymentUrl":"http://localhost:5000/process/payments/initiate/stk/push",
        "downloadUrl":"http://localhost:5000/process/v1/downloads/download",

    },
    "developer":{
        "portfolio":"https://tevinly.herokuapp.com/",
        "social":{
            "facebook":"https://facebook.com/kevy.kibbz",
            "twitter":"https://twitter.com/kevin36285655",
            "instagram":"https://instagram.com/ke_vin4578",
        }
    }
}
export default endpoints