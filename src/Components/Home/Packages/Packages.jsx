const packages={
    "site_packages":{
        "lite":{
            "package_name":"lite",
            "price":1
        },
        "pro":{
            "package_name":"pro",
            "price":2500
        }
    },
}
export default packages