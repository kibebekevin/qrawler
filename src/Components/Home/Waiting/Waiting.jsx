import React, { useEffect, useState } from "react";
import { auth, db } from "../../../Config/Config";
import { message } from "react-message-popup";
import Loader from "../../Home/Pay/Success/Loader";
import { useDispatch } from "react-redux";
import { WaitingModal } from "../../../Redux/actions/WaitingModal";
import { CloseWaitingModal } from "../../../Redux/actions/CloseWaitingModal";
import { updateDoc, doc } from "firebase/firestore";
import BackButton from "../Buttons/BackButton";

function Waiting() {
  const dispatch = useDispatch();
  const [isEmailVerified, setIsEmailVerified] = useState(false);

  useEffect(() => {
    const checkEmailVerification = async () => {
      const user = auth.currentUser;
      if (user) {
        await user.reload().catch((error)=>{
          const errorCode = error.code;
          if (errorCode === "auth/network-request-failed") {
            message.error("No network connection");
          }
        });
        if (user.emailVerified) {
          const updateUser = async (e) => {
            const getUser = doc(db, "users", auth.currentUser.uid);
            await updateDoc(getUser, {
              IsEmailVerified: true,
            })
              .then(() => {
                const currentUser = JSON.parse(localStorage.getItem("user"));
                const updatedUser = { ...currentUser, IsEmailVerified: true };
                localStorage.setItem("user", JSON.stringify(updatedUser));
                setIsEmailVerified(true);
                dispatch(CloseWaitingModal());
              })
              .catch((err) => {
                message.error("Something went wrong.");
              });
          };
          updateUser();
        } else {
          dispatch(WaitingModal());
        }
      }
    };
    checkEmailVerification();
  }, [dispatch]);

  const handleButtonClick = async () => {
    if (auth.currentUser != null && auth.currentUser.emailVerified) {
      const getUser = doc(db, "users", auth.currentUser.uid);
      await updateDoc(getUser, {
        IsEmailVerified: true,
      })
        .then(() => {
          const currentUser = JSON.parse(localStorage.getItem("user"));
          const updatedUser = { ...currentUser, IsEmailVerified: true };
          localStorage.setItem("user", JSON.stringify(updatedUser));
          setIsEmailVerified(true);
          dispatch(WaitingModal());
          message.success("Email verified");
        })
        .catch((err) => {
          message.error("Something went wrong.");
        });
    } else {
      message.error("Verify your email.");
    }
  };

  return (
    <div
      className="snippet-body"
      style={{ position: "absolute", top: "20%" }}
    >
      <div className="card p-3">
        {!isEmailVerified ? (
          <>
            <Loader text="Awaiting email verification..." />
            <div className="buttons">
              <button
                type="button"
                className="button"
                style={{ margin: "auto" }}
                onClick={handleButtonClick}
              >
                Continue
              </button>
            </div>
          </>
        ) : (
          <div>
          <header>
            <h4 className=" text-success">
              <i className="fa fa-check-circle-o"></i> Success
            </h4>
          </header>
          <main>
            <p>Email verified successfully.</p>
          </main>
          <div className="d-flex justify-content-center">
            <div className="buttons">
              <BackButton type="7" text="Ok"/>
            </div>
          </div>
        </div>
        )}
      </div>
    </div>
  );
}

export default Waiting;
