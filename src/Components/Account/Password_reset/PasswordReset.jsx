import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { message } from "react-message-popup";
import Validation from "./Validation";
import BackButton from "../../Home/Buttons/BackButton";
import { auth } from "../../../Config/Config";
import { signInWithEmailAndPassword, updatePassword } from "firebase/auth";

function PasswordReset() {
  const [errors, setErrors] = useState(false);
  const [disabledButton, setDisabledButton] = useState(true);

  const [values, setValues] = useState({
    oldpassword: "",
    newpassword: "",
    confirmnewpassword: "",
  });

  const [loading, setLoading] = useState(false);

  const handleInput = (event) => {
    const { name, value } = event.target;

    setValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    // Perform validation for the specific input field
    const fieldErrors = Validation({ ...values, [name]: value });
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: fieldErrors[name],
    }));
    setDisabledButton(Object.keys(errors).length > 0);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    setErrors(Validation(values));

    // Check for form errors
    const formErrors = Validation(values);
    if (Object.keys(formErrors).length > 0) {
      setLoading(false);
      message.error("Check your inputs");
      setDisabledButton(true);
      return;
    }
    
    setDisabledButton(false);
    setLoading(true);
    try {
      const user = auth.currentUser;
      const { oldpassword, newpassword } = values;

      /*Reauthenticate the user with their current password*/
      signInWithEmailAndPassword(auth, user.email, oldpassword)
        .then(async () => {
          setLoading(false);
          await updatePassword(user, newpassword);
          e.target.reset();
          message.success("Password changed successfully!");
        })
        .catch((error) => {
          setLoading(false);
          const errorCode = error.code;
          if (errorCode === "auth/network-request-failed") {
            message.error("No network connection");
          }else if (errorCode === "auth/wrong-password") {
            message.error("Wrong old password");
          } else {
            message.error("Please try again.");
          }
        });
    } catch (error) {
      setLoading(false);
      setDisabledButton(true);
      message.error("Please try again.");
    }
  };

  useEffect(() => {
    if (
      values.oldpassword === "" ||
      values.newpassword === "" ||
      values.confirmnewpassword === ""
    ) {
      setDisabledButton(true);
    } else {
      setDisabledButton(false);
    }
    return () => {
      setLoading(false);
    };
  }, [errors, values]);
  return (
    <div className="snippet-body" style={{ position: "absolute", top: "-40%" }}>
      <div className="card p-3">
        <h3 className="text-center">|Change Password</h3>
        <form
          method="post"
          onSubmit={handleSubmit}
          className="online-offline-page"
        >
          <div className="input-field">
            <i className="fa fa-lock" />
            <input
              type="password"
              placeholder="Old Password"
              name="oldpassword"
              onChange={handleInput}
              value={values.oldpassword}
            />
            {errors.oldpassword && (
              <small className="text-danger error-text">
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.oldpassword}
              </small>
            )}
          </div>
          <br />
          <div className="input-field">
            <i className="fa fa-lock" />
            <input
              type="password"
              placeholder="New Password"
              name="newpassword"
              onChange={handleInput}
              value={values.newpassword}
            />
            {errors.newpassword && (
              <small className="text-danger error-text">
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.newpassword}
              </small>
            )}
          </div>
          <br />
          <div className="input-field">
            <i className="fa fa-lock" />
            <input
              type="password"
              placeholder="Confirm new Password"
              name="confirmnewpassword"
              onChange={handleInput}
              value={values.confirmnewpassword}
            />
            {errors.confirmnewpassword && (
              <small className="text-danger error-text">
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.confirmnewpassword}
              </small>
            )}
          </div>
          <br />
          <div
            className=" align-items-center justify-content-center text-center d-flex"
            style={{ margin: " 0 auto !important" }}
          >
            <div className="buttons">
              <BackButton type="6" text="Close" />
              {loading || disabledButton ? (
                <button type="button" className="button" disabled>
                  {disabledButton ? "Submit" : "Please wait..."}
                </button>
              ) : (
                <button type="submit" className="button">
                  Submit
                </button>
              )}
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default PasswordReset;
