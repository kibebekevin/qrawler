export default function Validation(values){

    const errors={}

    if(values.oldpassword === ""){
        errors.oldpassword="Enter your old password"
    }
    
    if(values.newpassword === ""){
        errors.newpassword="Enter your new  password"
    }else if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,}$/.test(values.newpassword)) {
        errors.newpassword =
          "Password must be at least six characters long, containing at least one uppercase letter, one lowercase letter, and one digit";
    }

    if(values.confirmnewpassword === ""){
        errors.confirmnewpassword="Confirm your new password"
    }else if (values.confirmnewpassword !== values.newpassword) {
        errors.confirmnewpassword = "Passwords do not match";
    }

    return errors
}