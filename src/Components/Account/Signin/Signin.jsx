import { useContext, useState, React, useEffect } from "react";
import { signInWithEmailAndPassword } from "firebase/auth";
import "react-toastify/dist/ReactToastify.css";
import { AuthContext } from "../../../Context/AuthContext";
import Signup from "../Signup/Signup";
import Social from "../SocalMedia/Social";
import LogPic from "../../../images/log.svg";
import { useNavigate } from "react-router-dom";
import { auth, db } from "../../../Config/Config";
import Validation from "./Validation";
import { message } from "react-message-popup";
import { getDoc, doc } from "firebase/firestore";
import { ToastContainer, toast } from "react-toastify";
import Password from "../Forgotten_Pasword/Password";
import { useSelector, useDispatch } from "react-redux";
import { PasswordSet } from "../../../Redux/actions/PasswordSet";
import GoogleOneTap from "./GoogleOneTap/GoogleOneTap";
import { VersionContext } from "../../Home/Version/VersionContext";
import endpoints from "../../Home/Endpoints/Endpoints";

function Signin() {
  const [signupClicked, setSignupClicked] = useState(false);
  const [disabledButton, setDisabledButton] = useState(true);
  const version = useContext(VersionContext);
  const content =
    '<p className="float-right">Forgotten password? <a style="color:#fff;text-decoration:underline" href="javascript:void(0);">Reset here</a></p>';
  const [errors, setErrors] = useState(false);
  const [values, setValues] = useState({ email: "", password: "" });
  const [loading, setLoading] = useState(false);
  const passwordSet = useSelector((state) => state.passwordSet);
  const verifyEmail = useSelector((state) => state.verifyEmail);
  const navigate = useNavigate();
  const signupPage = () => {
    setSignupClicked(!signupClicked);
  };

  const handleInput = (event) => {
    const { name, value } = event.target;
  
    setValues((prevValues) => ({
      ...prevValues,
      [name]: value
    }));
  
    // Perform validation for the specific input field
    const fieldErrors = Validation({ ...values, [name]: value });
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: fieldErrors[name]
    }));
    setDisabledButton(Object.keys(errors).length > 0);

  };

  const { dispatch } = useContext(AuthContext);
  const dispatcher = useDispatch();
  const { currentUser } = useContext(AuthContext);

  const handleLogin = (e) => {
    e.preventDefault();
    setLoading(true);
    setErrors(Validation(values));

    // Check for form errors
    const formErrors = Validation(values);
    if (Object.keys(formErrors).length > 0) {
      setLoading(false);
      message.error("Check your inputs");
      setDisabledButton(true);
      return;
    }

    setDisabledButton(false)
    signInWithEmailAndPassword(auth, values.email, values.password)
      .then(async (userCredential) => {
        // Signed in
        const user = userCredential.user;
        const userDocRef = doc(db, "users", user.uid);
        const userDocSnap = await getDoc(userDocRef);
        const userData = userDocSnap.exists() ? userDocSnap.data() : null;
        setLoading(false);
        e.target.reset();
        const payload = {
          UID:user.uid,
          Email:user.email,
          Name:user.displayName,
          IsEmailVerified:user.emailVerified,
          IsAnonymous:user.isAnonymous,
          FreeTrialExpired: userData.FreeTrialExpired,
          HasPaid: userData.HasPaid,
          IsAccountActive: userData.IsAccountActive,
          IsSoftwareDownloded: userData.IsSoftwareDownloded,
          Phone: userData.Phone,
          PhotoUrl:userData.PhotoUrl,
          ProviderName:userData.ProviderName,
        };
        dispatch({ type: "Login", payload: payload });
        navigate("/dashboard");
        message.success("Logged in succesfully.");
      })
      .catch((error) => {
        setLoading(false);
        const errorCode = error.code;
        if (errorCode === "auth/user-not-found") {
          message.error("Wrong email address");
        }else if (errorCode === "auth/user-disabled") {
          message.error("Account is currently disabled");
        }else if (errorCode === "auth/wrong-password") {
          message.error("Wrong password");
          toast.info(
            <div
              dangerouslySetInnerHTML={{ __html: content }}
              onClick={handleClick}
            />
          );
        } else if (errorCode === "auth/network-request-failed") {
          message.error("No network connection");
        }else {
          message.error("Something went wrong.");
        }
      });
  };

  useEffect(() => {
    if (values.email ==="" || values.password ==="") {
      setDisabledButton(true);
    } else {
      setDisabledButton(false);
    }
    if (currentUser != null) {
      navigate("/dashboard");
    }

    return ()=>{
      setLoading(false)
    }
  }, [currentUser, navigate,values]);

  const handleClick = () => {
    dispatcher(PasswordSet());
  };

  return (
    <div
      className={`main-container ${
        signupClicked || verifyEmail ? "sign-up-mode" : ""
      }`}
    >
      <div>

        {currentUser==null && <GoogleOneTap/>}
        <ToastContainer
          position="top-right"
          autoClose={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          theme="colored"
        />{" "}
      </div>
      <div className="forms-container">
        <div className="signin-signup">
          <Signup />
          {passwordSet ? (
            <Password />
          ) : (
            <form method="post" onSubmit={handleLogin} className="sign-in-form">
              <h2 className="title">|Sign in</h2>
              <div className="input-field">
                <i className="fa fa-user" />
                <input
                  className="emailField"
                  type="email"
                  placeholder="Email address"
                  name="email"
                  value={values.email}
                  onChange={handleInput}
                />
                 {errors.email && (
                <small className="text-danger error-text">
                  <i className="text-danger fa fa-exclamation-circle"></i> {errors.email}
                </small>
              )}
              </div>
                <br/>
              <div className="input-field">
                <i className="fa fa-lock" />
                <input
                  className="passwordField"
                  type="password"
                  placeholder="Password"
                  name="password"
                  value={values.password}
                  onChange={handleInput}
                />
                {errors.password && (
                <small className="text-danger error-text">
                  <i className="text-danger fa fa-exclamation-circle"></i> {errors.password}
                </small>
              )}
              </div>
              <br/>
              {loading || disabledButton ? (
                <input
                  type="submit"
                  value={disabledButton ? "Login" : "Please wait..."}
                  className="btn"
                  disabled="disabled"
                />
              ) : (
                <input className="btn" type="submit" value="Login" />
              )}
              <p className="social-text">Or Sign up with social platforms</p>
              <Social />
            </form>
          )}
          <div className="copy-rights">
            <p>
              &copy; {new Date().getFullYear()} All rights reserved | Design by{" "}
              <a
                href={endpoints.developer.portfolio}
                rel="noreferrer"
                target="_blank"
              >
                Devme
              </a>
            </p>
            <p className="text-center">{version}</p>
          </div>
        </div>
      </div>

      <div className="panels-container">
        <div className="panel left-panel">
          <div className="content">
            <h2 style={{ fontSize: "60px" }}>
              {process.env.REACT_APP_SITE_NAME}
            </h2>
            <h3>New here ?</h3>
            <p>
              Sign up today and enjoy our services.Great things are awaiting
              you.
            </p>
            <button className="btn transparent" onClick={signupPage}>
              Sign up
            </button>
          </div>
          <img src={LogPic} className="image" alt="Background cover " />
        </div>
        <div className="panel right-panel">
          <div className="content">
            <h2 style={{ fontSize: "60px" }}>
              {process.env.REACT_APP_SITE_NAME}
            </h2>
            <h3>One of us ?</h3>
            <p>Sign in here</p>
            <button className="btn transparent" onClick={signupPage}>
              Sign in
            </button>
          </div>
          <img src="img/register.svg" className="image" alt="" />
        </div>
      </div>
    </div>
  );
}

export default Signin;
