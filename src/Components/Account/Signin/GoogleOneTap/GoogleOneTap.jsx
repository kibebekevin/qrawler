import React, { useEffect, useContext } from "react";
import { auth, db } from "../../../../Config/Config";
import { AuthContext } from "../../../../Context/AuthContext";
import { useNavigate } from "react-router-dom";
import { message } from "react-message-popup";
import { getDoc, doc, setDoc } from "firebase/firestore";
import { GoogleAuthProvider, signInWithCredential } from "firebase/auth";

function GoogleOneTap() {
  const { dispatch } = useContext(AuthContext);
  const navigate = useNavigate();

  function generateUniqueId(length) {
    let result = "";
    const characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const charactersLength = characters.length;

    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
  }

  useEffect(() => {
    const handleCredentialResponse = (response) => {
      if (response) {
        const credential = GoogleAuthProvider.credential(response.credential);
        // Sign in with the credential
        signInWithCredential(auth, credential)
          .then((userCredential) => {
            // User signed in successfully
            const user = userCredential.user;

            // Save the user data to Firebase Firestore
            liasFirestore(user);
          })
          .catch((error) => {
            console.error("Error signing in with credential:", error);
          });
      } else {
        console.error("Google id_token not found in the response.");
      }
    };

    const liasFirestore = async (user) => {
      try {
        let socialMediaProfile
        let loginProvider
        const userDocRef = doc(db, "users", user.uid);
        const userDocSnap = await getDoc(userDocRef);
        const providerData = user.providerData;
        // Check if providerData exists and is not empty
        if (Array.isArray(providerData) && providerData.length > 0) {

          // Check if the providerData array contains any social media provider information
          const usedSocialMedia = providerData.some(
            (provider) =>
              provider.providerId === "google.com" ||
              provider.providerId === "twitter.com" ||
              provider.providerId === "facebook.com"
          );
          if (usedSocialMedia) {
            socialMediaProfile = providerData.find(
              (provider) =>
                provider.providerId === "google.com" ||
                provider.providerId === "twitter.com" ||
                provider.providerId === "facebook.com"
            ).photoURL;
    
            loginProvider = providerData.find((provider) =>
              ["google.com", "twitter.com", "facebook.com"].includes(
                provider.providerId
              )
            ).providerId;
          }
        }

        if (!userDocSnap.exists()) {
          await setDoc(doc(db, "users", user.uid), {
            UserId: user.uid,
            FirstName: user.displayName,
            Email: user.email,
            Type: "Client",
            PhotoUrl:socialMediaProfile,
            ProviderName:loginProvider,
            IsAccountActive: false,
            IsSoftwareDownloded: false,
            IsSoftwareLatest: false,
            IsEmailVerified: false,
            TerminateProcess: false,
            DownloadAgain:false,
            SoftwareVersion:"v1.1.0",
            DateJoined: new Date(),
            TransactionDate:new Date(),
            RefreshSpeed: 3,
            Phone: "",
            ExpiryDays:7,
            VerbitEmail: user.email,
            VerbitPassord:"1234567",
            SoftwareId: generateUniqueId(12),
            Bio: "Something about you...",
            Client: true,
            Status: "Active",
            Package:"Free Version",
            HasPaid: false,
            FreeTrialExpired: false,
            Password: "",
          })
            .then(async () => {
              finalize(user,socialMediaProfile,loginProvider);
            })
            .catch((e) => {
              console.log("error one", e);
              message.error("Unknown error occurred");
            });
        } else {
          finalize(user,socialMediaProfile,loginProvider);
        }
      } catch (error) {
        console.error("Error checking document existence:", error);
      }
    };


    const finalize = (user,socialMediaProfile,loginProvider) => {
      const payload = {
        UID: user.uid,
        Email: user.email,
        Name: user.displayName,
        IsEmailVerified: true,
        IsAnonymous: user.isAnonymous,
        FreeTrialExpired: false,
        HasPaid: false,
        IsAccountActive: false,
        IsSoftwareDownloded: false,
        Phone: "",
        PhotoUrl:socialMediaProfile,
        ProviderName:loginProvider
      };
      message.success(`Logged in with ${loginProvider}.`);
      dispatch({ type: "Login", payload: payload });
      navigate("/dashboard");
      
    };
    
    if (window.google?.accounts?.id) {
      window.google.accounts.id.initialize({
        client_id:process.env.REACT_APP_GOOGLE_CLIENT_ID,
        callback: handleCredentialResponse,
      });
      window.google.accounts.id.prompt((notification) => {
        console.log("notification:",notification);
     });
    } else {
      console.error("Google One Tap API is not loaded.");
    }
  }, [dispatch, navigate]);
  return <div id="g_id_onload" />;
}

export default GoogleOneTap;
