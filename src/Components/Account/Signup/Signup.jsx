import { useContext, useState, React, useEffect } from "react";
import { message } from "react-message-popup";
import Validation from "./Validation";
import {
  createUserWithEmailAndPassword,
  updateProfile,
  sendEmailVerification,
} from "firebase/auth";
import { auth, db } from "../../../Config/Config";
import { setDoc, doc, getDoc } from "firebase/firestore";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../../Context/AuthContext";
import Loader from "../../Home/Pay/Success/Loader";
import "./Style.css";
import { useDispatch } from "react-redux";
import { WaitingModal } from "../../../Redux/actions/WaitingModal";
import generateUniqueId from "../../../key/UniqueId";

function Signup() {
  const { dispatch } = useContext(AuthContext);
  const dispatcher = useDispatch();
  const [disabledButton, setDisabledButton] = useState(true);
  const uniqueId = generateUniqueId(12);
  const [errors, setErrors] = useState(false);
  const [isVerified, setIsVerified] = useState(false);
  const navigate = useNavigate();
  const [values, setValues] = useState({ name: "", email: "", password: "" });

  const [loading, setLoading] = useState(false);

  const handleInput = (event) => {
    const { name, value } = event.target;

    setValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    // Perform validation for the specific input field
    const fieldErrors = Validation({ ...values, [name]: value });
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: fieldErrors[name],
    }));
    setDisabledButton(Object.keys(errors).length > 0);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    setErrors(Validation(values));

    // Check for form errors
    const formErrors = Validation(values);
    if (Object.keys(formErrors).length > 0) {
      setLoading(false);
      message.error("Check your inputs");
      setDisabledButton(true);
      return;
    }

    const email = values.email.toLowerCase();
    const displayName = values.name.toLowerCase();
    setDisabledButton(false);
    createUserWithEmailAndPassword(auth, email, values.password)
      .then(async (userCredential) => {
        const user = userCredential.user;
        await updateProfile(user, { displayName: displayName })
          .then(async () => {
            await setDoc(doc(db, "users", user.uid), {
              UserId: user.uid,
              FirstName: user.displayName,
              Email: user.email,
              Type: "Client",
              IsAccountActive: false,
              IsSoftwareDownloded: false,
              DownloadAgain: false,
              IsSoftwareLatest: false,
              RefreshSpeed: 3,
              IsEmailVerified: false,
              DateJoined: new Date(),
              TransactionDate: new Date(),
              TerminateProcess: false,
              ExpiryDays: 7,
              VerbitEmail: user.email,
              VerbitPassord: values.password,
              Phone: "",
              PhotoUrl:"",
              ProviderName:"email/password",
              SoftwareId: uniqueId,
              Package: "Free Version",
              SoftwareVersion: "v1.1.0",
              Bio: "Something about you...",
              Client: true,
              Status: "Active",
              HasPaid: false,
              FreeTrialExpired: false,
              Password: values.password,
            })
              .then(async () => {
                setLoading(false);
                e.target.reset();
                await sendEmailVerification(user)
                  .then(async () => {
                    const userDocRef = doc(db, "users", user.uid);
                    const userDocSnap = await getDoc(userDocRef);
                    const userData = userDocSnap.exists()
                      ? userDocSnap.data()
                      : null;
                    message.success("Account created succesfully.");
                    message.success("Verification link sent!");
                    setIsVerified(true);
                    const payload = {
                      UID: user.uid,
                      Email: user.email,
                      Name: user.displayName,
                      EmailVerified: user.emailVerified,
                      PhotoUrl:"",
                      ProviderName:"email/password",
                      IsAnonymous: user.isAnonymous,
                      FreeTrialExpired: userData.FreeTrialExpired,
                      HasPaid: userData.HasPaid,
                      IsAccountActive: userData.IsAccountActive,
                      IsSoftwareDownloded: userData.IsSoftwareDownloded,
                      Phone: userData.Phone,
                    };
                    dispatcher(WaitingModal());
                    dispatch({ type: "Login", payload: payload });
                    navigate("/dashboard");
                  })
                  .catch((err) => {
                    console.log(err);
                    message.error("Error while sending verification link");
                  });
              })
              .catch((e) => {
                console.log("Error:", e);
                setLoading(false);
                message.error("Uknown error ocurred");
              });
          })
          .catch((e) => {
            console.log("error two", e);
            setLoading(false);
            message.error("Uknown error ocurred");
          });
      })
      .catch((error) => {
        setLoading(false);
        const errorCode = error.code;
        if (errorCode === "auth/email-already-in-use") {
          message.error("Email address already in use");
        } else if (errorCode === "auth/weak-password") {
          message.error("Please use a strong password");
        } else if (errorCode === "auth/network-request-failed") {
          message.error("No network connection");
        }
      });
  };

  const handleVerifyButtonClick = async () => {};

  useEffect(() => {
    if (values.name === "" || values.email === "" || values.password === "") {
      setDisabledButton(true);
    } else {
      setDisabledButton(false);
    }
    if (auth.currentUser != null && auth.currentUser.emailVerified) {
      setIsVerified(true);
      dispatcher(WaitingModal());
    } else {
      setIsVerified(false);
    }
    return () => {
      setLoading(false);
    };
  }, [dispatcher, values]);
  return (
    <>
      {!isVerified ? (
        <form className="sign-up-form" method="post" onSubmit={handleSubmit}>
          <h2 className="title">|Sign up</h2>
          <div className="input-field">
            <i className="fa fa-user" />
            <input
              type="text"
              placeholder="Full Name"
              value={values.name}
              name="name"
              onChange={handleInput}
            />
            {errors.name && (
              <small className="text-danger error-text">
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.name}
              </small>
            )}
          </div>
          <br />
          <div className="input-field">
            <i className="fa fa-envelope" />
            <input
              type="email"
              placeholder="Email address"
              value={values.email}
              name="email"
              onChange={handleInput}
            />
            {errors.email && (
              <small className="text-danger error-text">
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.email}
              </small>
            )}
          </div>
          <br />
          <div className="input-field">
            <i className="fa fa-lock" />
            <input
              type="password"
              placeholder="Password"
              name="password"
              value={values.password}
              onChange={handleInput}
            />
            {errors.password && (
              <small
                className="text-danger error-text"
                style={{ marginLeft: "10px !important" }}
              >
                <i className="text-danger fa fa-exclamation-circle"></i>{" "}
                {errors.password}
              </small>
            )}
          </div>
          <br />
          {loading || disabledButton ? (
            <input
              type="submit"
              value={disabledButton ? "Submit" : "Please wait..."}
              className="btn"
              disabled="disabled"
            />
          ) : (
            <input type="submit" className="btn" value="Sign up" />
          )}
        </form>
      ) : (
        <>
          <form className="sign-up-form">
            <Loader text="Awaiting email verification..." size={100} />
            <br />
            {loading ? (
              <input
                type="submit"
                value="Please wait..."
                className="btn"
                disabled="disabled"
              />
            ) : (
              <input
                type="button"
                className="btn"
                value="Continue"
                onClick={handleVerifyButtonClick}
              />
            )}
          </form>
        </>
      )}
    </>
  );
}

export default Signup;
