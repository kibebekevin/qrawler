export default function Validation(values) {
    const errors = {};
    const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
    if (values.name === "") {
      errors.name = "Your name is required";
    }
  
    if (values.email === "") {
      errors.email = "Enter your email address";
    } else if (emailRegex.test(values.email) === false) {
      errors.email = "Enter a valid email address";
    }
  
    if (values.password === "") {
      errors.password = "Password is required";
    } else {
      let missingRequirements = [];
  
      if (!/(?=.*[a-z])/.test(values.password)) {
        missingRequirements.push("at least 1 lowercase letter");
      }
      if (!/(?=.*[A-Z])/.test(values.password)) {
        missingRequirements.push("at least 1 uppercase letter");
      }
      if (!/(?=.*\d)/.test(values.password)) {
        missingRequirements.push("at least 1 digit");
      }
  
      if (missingRequirements.length > 0) {
        let requirements = missingRequirements.length === 1 ? "requirement" : "requirements";
        let missing = missingRequirements.join(", ");
        errors.password = `Password should contain ${missing}.`;
        if (missingRequirements.length < 3) {
          errors.password += ` It is missing ${requirements}.`;
        } else {
          errors.password += ` It is missing these ${requirements}: ${missing}.`;
        }
      } else if (values.password.length < 6) {
        errors.password = "Password must be at least 6 characters long";
      }
    }
  
    return errors;
  }
  