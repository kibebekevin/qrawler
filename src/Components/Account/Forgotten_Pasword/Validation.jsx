export default function Validation(values){

    const errors={}
    const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(values.email === ""){
        errors.email="Enter your email address"
    }else if(emailRegex.test(values.email) === false){
        errors.email="Enter a valid email address"  
    }


    return errors
}