import React, { useState, useEffect } from "react";
import Validation from "./Validation";
import { sendPasswordResetEmail } from "firebase/auth";
import { auth } from "../../../Config/Config";
import { message } from "react-message-popup";
import { useDispatch } from "react-redux";
import { PasswordAction } from "../../../Redux/actions/PasswordAction";

function Password() {
  const [errors, setErrors] = useState(false);
  const [values, setValues] = useState({ email: "" });
  const [disabledButton, setDisabledButton] = useState(true);
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const handleInput = (event) => {
    const { name, value } = event.target;

    setValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));

    // Perform validation for the specific input field
    const fieldErrors = Validation({ ...values, [name]: value });
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: fieldErrors[name],
    }));
    setDisabledButton(Object.keys(errors).length > 0);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    setErrors(Validation(values));

    // Check for form errors
    const formErrors = Validation(values);
    if (Object.keys(formErrors).length > 0) {
      setLoading(false);
      message.error("Check your inputs");
      setDisabledButton(true);
      return;
    }
    setDisabledButton(false);
    await sendPasswordResetEmail(auth, values.email)
      .then(() => {
        /*password reset link sent*/
        setLoading(false);
        dispatch(PasswordAction());
        message.success("Password Reset Link sent.");
        message.info("Check your email.");
        e.target.reset();
      })
      .catch((error) => {
        setLoading(false);
        const errorCode = error.code;
        if (errorCode === "auth/network-request-failed") {
          message.error("No network connection");
        } else {
          message.error(
            "There was an error while sending password reset link."
          );
        }
      });
  };

  const handleCancel = (e) => {
    dispatch(PasswordAction());
  };

  useEffect(() => {
    if (values.email === "") {
      setDisabledButton(true);
    } else {
      setDisabledButton(false);
    }
    return () => {
      setLoading(false);
    };
  }, [values]);

  return (
    <>
      <form method="post" onSubmit={handleSubmit} className="sign-in-form">
        <h2 className="title">|Forgotten Password</h2>
        <div className="input-field">
          <i className="fa fa-envelope" />
          <input
            type="email"
            placeholder="Email address"
            name="email"
            value={values.email}
            onChange={handleInput}
          />
          {errors.email && (
            <small className="text-danger error-text">
              <i className="text-danger fa fa-exclamation-circle"></i>{" "}
              {errors.email}
            </small>
          )}
        </div>
        <br />
        <div
          className="buttoners align-items-center justify-content-center text-center d-flex"
          style={{ margin: " 0 auto !important" }}
        >
          <input
            className="btn"
            type="button"
            value="Cancel"
            onClick={handleCancel}
          />
          {"  "}
          {loading || disabledButton ? (
            <input
              type="submit"
              value={disabledButton ? "Submit" : "Please wait..."}
              className="btn"
              disabled="disabled"
            />
          ) : (
            <input className="btn" type="submit" value="Submit" />
          )}
        </div>
      </form>
    </>
  );
}

export default Password;
