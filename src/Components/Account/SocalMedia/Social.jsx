import React, { useContext } from "react";
import { auth, db } from "../../../Config/Config";
import { message } from "react-message-popup";
import { getDoc, doc, setDoc } from "firebase/firestore";
import {
  signInWithPopup,
  GoogleAuthProvider,
  TwitterAuthProvider,
  FacebookAuthProvider,
} from "firebase/auth";
import { AuthContext } from "../../../Context/AuthContext";
import { useNavigate } from "react-router-dom";

function Social() {
  const navigate = useNavigate();
  const { dispatch } = useContext(AuthContext);
  function generateUniqueId(length) {
    let result = "";
    const characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const charactersLength = characters.length;

    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
  }
  const handleGoogleSignIn = () => {
    const provider = new GoogleAuthProvider(auth);
    try {
      signInWithPopup(auth, provider)
        .then(async (result) => {
          // Handle successful sign-in
          const user = result.user;
          liasFirestore(user);
        })
        .catch((error) => {
          // Handle sign-in error
          console.log("error while login with google:",error)
          const errorCode = error.code;
          if (
            errorCode === "auth/network-request-failed" ||
            errorCode === "auth/internal-error"
          ) {
            message.error("No network connection");
          } else if (errorCode === "auth/user-disabled") {
            message.info("Account is currently disabled");
          } else if (errorCode === "auth/cancelled-popup-request") {
            message.info("Popup request cancelled");
          } else {
            message.error("Unknown error ocurred");
          }
        });
    } catch (error) {
      message.error("Unknown error ocurred");
    }
  };

  const handleFacebookSignIn = (e) => {
    const provider = new FacebookAuthProvider();
    try {
      signInWithPopup(auth, provider)
        .then((result) => {
          // Successful login, do something with the user data
          const user = result.user;
          liasFirestore(user);
        })
        .catch((error) => {
          // Handle login error
          const errorCode = error.code;
          if (
            errorCode === "auth/network-request-failed" ||
            errorCode === "auth/internal-error"
          ) {
            message.error("No network connection");
          } else if (errorCode === "auth/cancelled-popup-request") {
            message.info("Popup request cancelled");
          } else if (errorCode === "auth/user-disabled") {
            message.info("Account is currently disabled");
          } else {
            message.error("Unknown error ocurred");
          }
        });
    } catch (error) {
      console.log(error);
      message.error("Unknown error ocurred");
    }
  };

  const handleTwitterSignIn = (e) => {
    const provider = new TwitterAuthProvider();
    try {
      signInWithPopup(auth, provider)
        .then((result) => {
          // Successful login, do something with the user data
          const user = result.user;
          liasFirestore(user);
        })
        .catch((error) => {
          // Handle login error
          const errorCode = error.code;
          console.log("error while login with twitter:",error)
          if (
            errorCode === "auth/network-request-failed" ||
            errorCode === "auth/internal-error"
          ) {
            message.error("No network connection");
          } else if (errorCode === "auth/user-disabled") {
            message.info("Account is currently disabled");
          } else if (errorCode === "auth/cancelled-popup-request") {
            message.info("Popup request cancelled");
          } else {
            message.error("Unknown error ocurred");
          }
        });
    } catch (error) {
      console.log(error);
      message.error("Unknown error ocurred");
    }
  };



  const liasFirestore = async (user) => {
    try {
      let socialMediaProfile
      let loginProvider
      const userDocRef = doc(db, "users", user.uid);
      const userDocSnap = await getDoc(userDocRef);
      const providerData = user.providerData;
      // Check if providerData exists and is not empty
      if (Array.isArray(providerData) && providerData.length > 0) {

        // Check if the providerData array contains any social media provider information
        const usedSocialMedia = providerData.some(
          (provider) =>
            provider.providerId === "google.com" ||
            provider.providerId === "twitter.com" ||
            provider.providerId === "facebook.com"
        );
        if (usedSocialMedia) {
          socialMediaProfile = providerData.find(
            (provider) =>
              provider.providerId === "google.com" ||
              provider.providerId === "twitter.com" ||
              provider.providerId === "facebook.com"
          ).photoURL;
  
          loginProvider = providerData.find((provider) =>
            ["google.com", "twitter.com", "facebook.com"].includes(
              provider.providerId
            )
          ).providerId;
        }
      }
      if (!userDocSnap.exists()) {
        await setDoc(doc(db, "users", user.uid), {
          UserId: user.uid,
          FirstName: user.displayName,
          Email: user.email,
          Type: "Client",
          PhotoUrl:socialMediaProfile,
          ProviderName:loginProvider,
          IsAccountActive: false,
          RefreshSpeed: 3,
          IsSoftwareDownloded: false,
          IsSoftwareLatest: false,
          VerbitEmail: user.email,
          DownloadAgain: false,
          VerbitPassord: "1234567",
          TerminateProcess: false,
          IsEmailVerified: true,
          SoftwareVersion: "v1.1.0",
          Package: "Free Version",
          ExpiryDays: 7,
          DateJoined: new Date(),
          TransactionDate: new Date(),
          Phone: "",
          SoftwareId: generateUniqueId(12),
          Bio: "Something about you...",
          Client: true,
          Status: "Active",
          HasPaid: false,
          FreeTrialExpired: false,
          Password: "",
        })
          .then(() => {
            finalize(user,socialMediaProfile,loginProvider);
          })
          .catch((e) => {
            message.error("uknown error ocurred");
          });
      } else {
        finalize(user,socialMediaProfile,loginProvider);
      }
    } catch (error) {
      console.error("Error checking document existence:", error);
    }
  };


  
  const finalize = (user,socialMediaProfile,loginProvider) => {
    const payload = {
      UID: user.uid,
      Email: user.email,
      Name: user.displayName,
      IsEmailVerified: true,
      IsAnonymous: user.isAnonymous,
      FreeTrialExpired: false,
      HasPaid: false,
      IsAccountActive: false,
      IsSoftwareDownloded: false,
      Phone: "",
      PhotoUrl:socialMediaProfile,
      ProviderName:loginProvider
    };
    message.success(`Logged in with ${loginProvider}.`);
    dispatch({ type: "Login", payload: payload });
    navigate("/dashboard");
    
  };
  return (
    <div className="social-media">
      <span className="social-icon" onClick={handleFacebookSignIn}>
        <i className="fa fa-facebook-f" />
      </span>
      <span className="social-icon" onClick={handleTwitterSignIn}>
        <i className="fa fa-twitter" />
      </span>
      <span className="social-icon" onClick={handleGoogleSignIn}>
        <i className="fa fa-google" />
      </span>
    </div>
  );
}

export default Social;
